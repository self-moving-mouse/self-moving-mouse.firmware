#include <stdint.h>
#include <stdio.h>

#include "debouncing.h"
#include "unity-test/src/unity.h"

void setUp()
{  
}

void tearDown()
{
}

#define DEBOUNCING_THRESHOLD_MS 10
#define LONG_BUTTON_PRESS_DURATION_MS 1000

const char* _getEventName(int8_t eventID)
{
    switch (eventID)
    {
        case NO_UNPROCESSED_EVENTS:
            return "NO_UNPROCESSED_EVENTS";
        case SHORT_BUTTON_PRESS:
            return "SHORT_BUTTON_PRESS";
        case LONG_BUTTON_PRESS:
            return "LONG_BUTTON_PRESS";
        default:
            return "<UNKNOWN>";
    }
}

void _runTest(
    uint8_t  Nsteps,
    const uint32_t timesteps[],
    const uint8_t  pinStates[],
    const int8_t   expectedEvents[])
{
    struct PinState pinState = {0};

    UpdatePinState(
        &pinState,
        pinStates[0],
        timesteps[0]);
    MarkStateAsProcessed(&pinState);

    char messageBuffer[256];
    for(uint8_t i=1; i<Nsteps; i++) {
        // Timer is incremented in steps 
        // much larger than main loop iteration
        // so the firmware will most likely
        // pass through multiple iterations
        // of main loop before either "time"
        // or button state will change.        
        for(uint8_t j=0; j<5; j++) {
            UpdatePinState(
                &pinState,
                pinStates[i],
                timesteps[i]);
            int8_t pinEvent =
                GetUnprocessedEvent(
                    &pinState,
                    DEBOUNCING_THRESHOLD_MS,
                    LONG_BUTTON_PRESS_DURATION_MS,
                    timesteps[i]);
            if(pinEvent != NO_UNPROCESSED_EVENTS) {
                MarkStateAsProcessed(&pinState);
            }   
            // Button events should be generated only 
            // on first iteration after time change
            int8_t expectedEvent = j==0?expectedEvents[i]:0;
            sprintf(
                messageBuffer, 
                "Got event %s(%d) instead of %s(%d) on step %d iteration %d", 
                _getEventName(pinEvent),
                pinEvent,
                _getEventName(expectedEvent),
                expectedEvent,
                i,
                j);
            TEST_ASSERT_EQUAL_INT8_MESSAGE(
                expectedEvent,
                pinEvent,
                messageBuffer);
        }
    }
}

#define test_IgnoringGlitches_N 6
const uint32_t test_IgnoringGlitches_time[test_IgnoringGlitches_N] = 
    {0L, 0L, 5L,  9L, 10L, 15L};
const uint8_t test_IgnoringGlitches_pinState[test_IgnoringGlitches_N] = 
    {1,   1,  0,   0,   1,   1};    
const int8_t test_IgnoringGlitches_events[test_IgnoringGlitches_N] = {
    NO_UNPROCESSED_EVENTS, 
    NO_UNPROCESSED_EVENTS, 
    NO_UNPROCESSED_EVENTS, 
    NO_UNPROCESSED_EVENTS, 
    NO_UNPROCESSED_EVENTS};
void test_IgnoringGlitches()
{
    _runTest(
        test_IgnoringGlitches_N,
        test_IgnoringGlitches_time,
        test_IgnoringGlitches_pinState,
        test_IgnoringGlitches_events);
}

#define test_ShortKeypress_NoGlitches_N 8
const uint32_t test_ShortKeypress_NoGlitches_time[test_ShortKeypress_NoGlitches_N] = 
    {0L, 0L, 5L, 20L, 200L, 209L, 210L, 220L};
const uint8_t test_ShortKeypress_NoGlitches_pinState[test_ShortKeypress_NoGlitches_N] = 
    {1,   1,  0,   0,    1,    1,    1,    1};
//                             ^     ^
//                             |     |- event here: debouncing threshold passed  
//                             |- no event here. Less than debouncing
//                                threshold from last state change              
const int8_t test_ShortKeypress_NoGlitches_events[test_ShortKeypress_NoGlitches_N] = 
    {0,   0,  0,   0,    0,    0,    1,    0};
void test_ShortKeypress_NoGlitches()
{
    _runTest(
        test_ShortKeypress_NoGlitches_N,
        test_ShortKeypress_NoGlitches_time,
        test_ShortKeypress_NoGlitches_pinState,
        test_ShortKeypress_NoGlitches_events);
}

#define test_LongKeypress_NoGlitches_N 13
const uint32_t test_LongKeypress_NoGlitches_time[test_LongKeypress_NoGlitches_N] = 
    {0L, 0L, 5L, 20L, 1004L, 1005L,  1005L, 1023L, 1025L, 1025L, 1034L, 1035L, 1040L};
const uint8_t test_LongKeypress_NoGlitches_pinState[test_LongKeypress_NoGlitches_N] = 
    { 1,  1,  0,   0,     0,     0,      0,     0,     1,     1,     1,     1,     1};
//            ^           ^      ^
//            |           |      |- event here: long press threshold passed
//            |           |- no event here. Less than long press threshold 
//            |              from last pin change
//            |- pin state change time
const int8_t test_LongKeypress_NoGlitches_events[test_LongKeypress_NoGlitches_N] = 
    { 0,  0,  0,   0,     0,     2,      0,     0,     0,     0,     0,     0,     0};
void test_LongKeypress_NoGlitches()
{
    _runTest(
        test_LongKeypress_NoGlitches_N,
        test_LongKeypress_NoGlitches_time,
        test_LongKeypress_NoGlitches_pinState,
        test_LongKeypress_NoGlitches_events);
}

#define test_ShortKeypress_Glitches_N 21
const uint32_t test_ShortKeypress_Glitches_time[test_ShortKeypress_Glitches_N] = 
    {0L, 0L, 5L, 10L, 15L, 20L, 25L, 30L, 35L, 40L, 45L, 50L, 55L, 60L, 65L, 70L, 75L, 80L, 85L, 90L, 95L};
const uint8_t test_ShortKeypress_Glitches_pinState[test_ShortKeypress_Glitches_N] = 
    { 1,  0,  1,   0,   0,   0,   1,   0,   0,   1,   0,   1,   0,   1,   1,   1,   1,   1,   0,   1,   1};  
const int8_t test_ShortKeypress_Glitches_events[test_ShortKeypress_Glitches_N] = 
    { 0,  0,  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   0,   0,   0,   0,   0};       
void test_ShortKeypress_Glitches()
{
    _runTest(
        test_ShortKeypress_Glitches_N,
        test_ShortKeypress_Glitches_time,
        test_ShortKeypress_Glitches_pinState,
        test_ShortKeypress_Glitches_events);
}

#define test_LongKeypress_Glitches_N 22
const uint32_t test_LongKeypress_Glitches_time[test_LongKeypress_Glitches_N] = 
    {0L, 0L, 5L, 10L, 15L, 20L, 25L, 30L, 35L, 40L,  1025L, 1030L, 1035L, 1040L, 1045L, 1050L, 1055L, 1060L, 1065L, 1070L, 1075L, 1080L};
const uint8_t test_LongKeypress_Glitches_pinState[test_LongKeypress_Glitches_N] = 
    { 1,  0,  1,   0,   0,   0,   1,   0,   0,   0,      0,     0,     1,     0,      1,     1,     1,     1,     1,     0,     1,     1};  
const int8_t test_LongKeypress_Glitches_events[test_LongKeypress_Glitches_N] = 
    { 0,  0,  0,   0,   0,   0,   0,   0,   0,   0,      0,     2,     0,     0,      0,     0,     0,     0,     0,     0,     0,     0};   
//                           ^         ^                        ^                                   ^
//                           |         |                        |                                   |- StateOld changed to 1 without event 
//                           |         |                        |                                      after long press
//                           |         |                        |- long press threshold passed from last change
//                           |         |- lastChangedTime updated here without updating StateOld
//                           |- StateOld changed to 0 here (debouncing threshold from last change)    
void test_LongKeypress_Glitches()
{
    _runTest(
        test_LongKeypress_Glitches_N,
        test_LongKeypress_Glitches_time,
        test_LongKeypress_Glitches_pinState,
        test_LongKeypress_Glitches_events);
}

int main(void)
{
  UNITY_BEGIN();
  RUN_TEST(test_IgnoringGlitches);
  RUN_TEST(test_ShortKeypress_NoGlitches);  
  RUN_TEST(test_LongKeypress_NoGlitches);
  RUN_TEST(test_ShortKeypress_Glitches);
  RUN_TEST(test_LongKeypress_Glitches);
  return UNITY_END();
}
