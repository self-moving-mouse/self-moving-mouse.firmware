/* ------------------------------------------------------------------------- */
/* V-USB based mouse emulation device firmware                               */
/* ------------------------------------------------------------------------- */


#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "usbconfig.h"
#include "v-usb/usbdrv/usbdrv.h"

#include "statusled.h"
#include "mouse.h"
#include "time.h"
#include "button.h"
#include "debouncing.h"
#include "microrng.h"
#include "settings.h"
#include "tracks-library.h"
#include "track-utils.h"

#define DELAY_BOUNDARY_INCREMENT_MS   5000L

#define TRACK_TSTEP_MS 8
#define DELAY_TSTEP_MS 10L
#define BLINK_LENGTH   500L

// see state diagram in
// main-states.plantuml, main-states.png
// 2   in documentation
#define DEVICE_MODE_STOP             0

#define DEVICE_MODE_FLAG_PLAYER      (1<<7)

// 3.1 in documentation
#define DEVICE_MODE_TRACK_SELECTION  (DEVICE_MODE_FLAG_PLAYER | 1)
#define DEVICE_MODE_PLAYING_TRACK    (DEVICE_MODE_FLAG_PLAYER | 2)
// 3.2 in documentation
#define DEVICE_MODE_DELAY_SELECTION  (DEVICE_MODE_FLAG_PLAYER | 3)
#define DEVICE_MODE_TRACK_DELAY      (DEVICE_MODE_FLAG_PLAYER | 4)

#define DEVICE_MODE_FLAG_CONFIG      (1<<6)

// 4.1 in documentation
#define DEVICE_MODE_CONFIG_MIN_DELAY (DEVICE_MODE_FLAG_CONFIG | 5)
// 4.2 in documentation
#define DEVICE_MODE_CONFIG_MAX_DELAY (DEVICE_MODE_FLAG_CONFIG | 6)

// These two have to be static
// because they are used in
// interrupt handlers
static ClockState clockState;
static RNGState rngState;

ISR(CLOCK_TIMER_INTERRUPT_VECT)
{
   updateTimer(&clockState);
}

ISR(WDT_vect)
{
   RNGOnWatchdog(&rngState, TCNT0);
}

int __attribute__((noreturn)) main(void)
{
    // current time is copied into this variable at
    // the beginning of each main loop iteration
    uint32_t currentTime;

    struct PinState button1;
    struct TrackPlayerState trackPlayerState;
    struct SelfMovingMouseSettings settings;
    
    // remembering last played track number here     
    // so that random track sequence can be selected
    // so that the same one never plays two times
    // in a row
    uint8_t previousTrackId = 0;
    
    // time for the only software
    // timer needed for this firmware
    uint32_t delayEndTime;

    uint8_t btnPressCounter;
    uint8_t blinkCounter;

    uint8_t deviceMode = DEVICE_MODE_STOP;
        
    configureRNG();
    configureStatusLED();
    configureButtons(&button1);
    configureTimer(&clockState);
    restoreSettingsFromEEPROM(&settings);
    usbInit();
    // enforcing re-enumeration,
    // this must be done while interrupts are disabled
    usbDeviceDisconnect();
    // fake USB disconnect for > 250 ms
    _delay_ms(250);
    usbDeviceConnect();
    sei();

    // main event loop
    for(;;){
        usbPoll();
        currentTime = millis(&clockState);

        // Button state is polled in main loop
        // to avoid using interrupt handler which
        // can interfere with V-USB timing
        updateButtonsState(&button1, currentTime);
        int8_t button1Event =
            getButton1Event(&button1, currentTime);

        uint8_t randomNumberReady =
            updateRandomSample(&rngState);

        // using inlined if .. else
        // saves code size compared to
        // switch
        if(deviceMode==DEVICE_MODE_STOP){
            statusLEDOff();
            if(button1Event != NO_UNPROCESSED_EVENTS){
                delayEndTime = 0;
                if(button1Event == LONG_BUTTON_PRESS){
                    btnPressCounter = 0;
                    blinkCounter = settings.minDelay;
                    deviceMode = DEVICE_MODE_CONFIG_MIN_DELAY;
                } else { //if(button1Event == SHORT_BUTTON_PRESS)
                    statusLEDOn();
                    deviceMode = DEVICE_MODE_TRACK_SELECTION;
                }
            }
        } else if (deviceMode & DEVICE_MODE_FLAG_CONFIG) {    
            // toggle status LED until the number of blinks 
            // set in blinkCounter is reached
            // or a button is pressed
            if((btnPressCounter == 0) &&
               (blinkCounter > 0) &&
               (currentTime >= delayEndTime)){
                
                if(getStatusLEDState()) {
                    blinkCounter--;
                    statusLEDOff();
                } else {
                    statusLEDOn();
                }                                        
                // equal on and off intervals
                delayEndTime = currentTime + BLINK_LENGTH;
            }
            
            if(button1Event != NO_UNPROCESSED_EVENTS) {
                if(button1Event == SHORT_BUTTON_PRESS) {
                    // Short button press handler is the same
                    // for both configuration states
                    if(btnPressCounter<255) {
                        btnPressCounter++;
                        statusLEDOff();
                    }
                } else {// button1Event == LONG_BUTTON_PRESS
                    switch(deviceMode)
                    {
                        case DEVICE_MODE_CONFIG_MIN_DELAY:
                            if(btnPressCounter>0) {
                                settings.minDelay = btnPressCounter;
                            }
                            btnPressCounter = 0;
                            delayEndTime = 0;
                            blinkCounter = settings.delayIntervalLength;
                            deviceMode = DEVICE_MODE_CONFIG_MAX_DELAY;
                            break;
                        case DEVICE_MODE_CONFIG_MAX_DELAY:
                            if(btnPressCounter>0) {
                                settings.delayIntervalLength = btnPressCounter;
                            }
                            saveSettingsToEEPROM(&settings);
                            deviceMode = DEVICE_MODE_STOP;
                            break;
                    }
                }
            }           
        } else {
            // all other states are related to playing tracks
            if(button1Event != NO_UNPROCESSED_EVENTS) {
                deviceMode = DEVICE_MODE_STOP;
            } else {
                switch(deviceMode)
                {
                    case DEVICE_MODE_TRACK_SELECTION:
                        // looping in DEVICE_MODE_TRACK_SELECTION
                        // state until "randomness"
                        // necessary for the next sample is
                        // accumulated
                        if(randomNumberReady) {
                            // https://stackoverflow.com/a/15734524
                            // Previous track number is incremented
                            // at least one and at most N-1 tracks
                            // together with final mod N operation
                            // this guarantees that no two consecutive
                            // tracks will be the same.
                            // This method uses two slow divisions
                            // but for our purposes this does not matter.
                            uint8_t track_id =
                                (previousTrackId +
                                (useRandomNumber(&rngState) % (N_TRACKS-1))
                                +1) % N_TRACKS;
                            setTrack(
                                &trackPlayerState,
                                tracks[track_id],
                                trackSizes[track_id]);
                            deviceMode = DEVICE_MODE_PLAYING_TRACK;
                        }
                        break;
                    case DEVICE_MODE_PLAYING_TRACK:
                        if(currentTime >= delayEndTime) {
                            if(playNextStep(&trackPlayerState)) {
                                delayEndTime = currentTime + TRACK_TSTEP_MS;
                            } else {
                                deviceMode = DEVICE_MODE_DELAY_SELECTION;
                            }
                        }
                        break;
                    case DEVICE_MODE_DELAY_SELECTION:
                        // looping in DEVICE_MODE_DELAY_SELECTION
                        // state until "randomness"
                        // necessary for the next sample is
                        // accumulated
                        if(randomNumberReady) {
                            delayEndTime =
                                currentTime +
                                DELAY_BOUNDARY_INCREMENT_MS * (uint32_t)settings.minDelay +
                                ((DELAY_BOUNDARY_INCREMENT_MS *
                                (uint32_t)settings.delayIntervalLength *
                                (uint32_t)useRandomNumber(&rngState)) / 255);
                            deviceMode = DEVICE_MODE_TRACK_DELAY;
                        }
                    case DEVICE_MODE_TRACK_DELAY:
                        if(currentTime>=delayEndTime) {
                            deviceMode = DEVICE_MODE_TRACK_SELECTION;
                        }
                        break;
                }
            }
        }
    }
}
