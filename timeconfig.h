#ifndef TIMECONFIG_HEADER_GUARD
#define TIMECONFIG_HEADER_GUARD

// For smooth tracks playback
// CLOCK_STEP_MS should be a multiple of
// TRACK_TSTEP_MS which is currently 8 ms
#define CLOCK_STEP_MS 4

#endif