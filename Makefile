# Based on
# http://arduino.stackexchange.com/questions/12114/basic-makefile-for-avr-gcc
# https://www.avrfreaks.net/forum/makefile-object-not-found-build-v-usb-studio

#----------------
# -- Compiling --
#----------------

CC=avr-gcc
OBJCOPY=avr-objcopy
OBJDUMP=avr-objdump
GCOV=gcov

AVRSIZE=avr-size
PROG_NAME=selfmoving-mouse

MCU_NAME_GCC=attiny85
F_CPU=16500000UL

DIRS =  build/tests
DIRS += build/firmware

RELEASE_O_PREFIX = build/firmware

C_SOURCES = main.c
C_SOURCES += mouse.c
C_SOURCES += button.c
C_SOURCES += debouncing.c
C_SOURCES += time.c
C_SOURCES += microrng.c
C_SOURCES += track-utils.c
C_SOURCES += v-usb/libs-device/osccal.c
C_SOURCES += v-usb/usbdrv/usbdrv.c

ASM_SOURCES = v-usb/usbdrv/usbdrvasm.S

CFLAGS = -Os -std=gnu99 -Wall -Wextra -I. -Iv-usb/usbdrv -Iv-usb/libs-device -Itracks-library
CFLAGS += -DF_CPU=${F_CPU}
CFLAGS += -mmcu=$(MCU_NAME_GCC)

COMPILE = $(CC) $(CFLAGS)

LDFLAGS = -Wl,-Map,${PROG_NAME}.map
LDFLAGS += -mmcu=$(MCU_NAME_GCC)

OBJECTS = $(addprefix $(RELEASE_O_PREFIX)/, $(notdir $(C_SOURCES:.c=.o)))
OBJECTS += $(addprefix $(RELEASE_O_PREFIX)/, $(notdir $(ASM_SOURCES:.S=.o)))

LISTINGS = $(addprefix $(RELEASE_O_PREFIX)/, $(notdir $(C_SOURCES:.c=.lss)))

# Generic rule for compiling C files:
$(RELEASE_O_PREFIX)/%.o: %.c
	$(COMPILE) -c $< -o $@

$(RELEASE_O_PREFIX)/%.o: v-usb/libs-device/%.c
	$(COMPILE) -c $< -o $@

$(RELEASE_O_PREFIX)/%.o: v-usb/usbdrv/%.c
	$(COMPILE) -c $< -o $@

# Generic rule for assembling Assembler source files:
$(RELEASE_O_PREFIX)/%.o:  v-usb/usbdrv/%.S
	$(COMPILE) -x assembler-with-cpp -c $< -o $@
# "-x assembler-with-cpp" should not be necessary since this is the default
# file type for the .S (with capital S) extension. However, upper case
# characters are not always preserved on Windows. To ensure WinAVR
# compatibility define the file type manually.

build: ${PROG_NAME}.hex ${PROG_NAME}.avr-size-report.txt

${PROG_NAME}.elf: $(OBJECTS)
	$(CC) $(LDFLAGS) $(LDLIBS) $^ -o $@
	${AVRSIZE} -C --mcu=$(MCU_NAME_GCC) ${PROG_NAME}.elf | tee ${PROG_NAME}.avr-size-report.txt

${PROG_NAME}.avr-size-report.txt: ${PROG_NAME}.elf

${PROG_NAME}.hex: ${PROG_NAME}.elf
	${OBJCOPY} -O ihex -j .text -j .data -R .eeprom $< $@

${PROG_NAME}.disasm: ${PROG_NAME}.hex
	$(OBJDUMP) -D -m avr ${PROG_NAME}.hex > ${PROG_NAME}.disasm

# Generic rule for writing preprocessing results
$(RELEASE_O_PREFIX)/%.lss: %.c
	$(COMPILE) -E $< -o $@

$(RELEASE_O_PREFIX)/%.lss: v-usb/libs-device/%.c
	$(COMPILE) -E $< -o $@

$(RELEASE_O_PREFIX)/%.lss: v-usb/usbdrv/%.c
	$(COMPILE) -E $< -o $@

disassemble: ${PROG_NAME}.disasm

list: $(LISTINGS)

size: ${PROG_NAME}.elf
	${AVRSIZE} -C --mcu=$(MCU_NAME_GCC) ${PROG_NAME}.elf

makedebug:
	@echo C_SOURCES=$(C_SOURCES)
	@echo ASM_SOURCES=$(ASM_SOURCES)
	@echo LDFLAGS=$(LDFLAGS)
	@echo LDLIBS=$(LDLIBS)
	@echo OBJECTS=$(OBJECTS)

#----------------
# -- Testing  --
#----------------

TEST_CC=gcc

TEST_CFLAGS=-O0 -std=gnu99 -ggdb -Wall -I. -Iunity-test/src
TEST_CFLAGS += -DF_CPU=${F_CPU}

TEST_COMPILE = $(TEST_CC) $(TEST_CFLAGS)

TEST_C_SOURCES = debouncing_test.c
TEST_C_SOURCES += debouncing.c

TEST_O_PREFIX = build/tests

TEST_OBJECTS = $(addprefix $(TEST_O_PREFIX)/, $(TEST_C_SOURCES:.c=.o))
TEST_OBJECTS += $(TEST_O_PREFIX)/unity.o

$(TEST_O_PREFIX)/%.o: %.c
	$(TEST_COMPILE) -c $< -o $@ -fprofile-arcs -ftest-coverage

$(TEST_O_PREFIX)/unity.o: unity-test/src/unity.c
	$(TEST_COMPILE) -c $< -o $@

${PROG_NAME}-test.elf: $(TEST_OBJECTS)
	$(TEST_CC) $(TEST_LDFLAGS) $(LDLIBS) -fprofile-arcs $^ -o $@

${PROG_NAME}.test-report.txt: ${PROG_NAME}-test.elf
	./${PROG_NAME}-test.elf | tee ${PROG_NAME}.test-report.txt

coverage: Coverage.xml Coverage.html Coverage.debouncing.c.html Coverage.txt

Coverage.xml: debouncing.c
	gcovr -x --xml-pretty --filter debouncing.c -o Coverage.xml

Coverage.html: debouncing.c
	gcovr --html-details --filter debouncing.c -o Coverage.html

Coverage.txt: debouncing.c
	gcovr --filter debouncing.c | tee Coverage.txt

Coverage.debouncing.c.html: Coverage.html

#%.c.gcov: %.c
#	$(GCOV) -obuild/tests $^


test: ${PROG_NAME}.test-report.txt

testmakedebug:
	@echo TEST_C_SOURCES=$(TEST_C_SOURCES)
	@echo OBJECTS=$(OBJECTS)

testdebug: ${PROG_NAME}-test.elf


#----------------
# -- Flashing  --
#----------------
MCU_NAME_AVRDUDE=ATtiny85
AVRDUDE_CMD_BASE=avrdude -c usbasp -b19200  -B10

install: ${PROG_NAME}.hex
	${AVRDUDE_CMD_BASE} -p ${MCU_NAME_AVRDUDE} \
	                    -U ${PROG_NAME}.hex


# https://www.engbedded.com/fusecalc/
# (comment below uses semantics of 0 and 1
# from datasheet that differs from fusecalc).
# 1 means unprogrammed
# 0 means programmed

# CKSEL      0001 - High Frequency PLL Clock (
#                   internal nominally 8 MHz
#                   oscillator x2)
# SUT      10     - slowly rising power
# CKOUT   1       - system clock output on the CLKO pin is OFF
# CKDIV8 1        - clock divided by 8 is OFF
#
#        11100001 = E1
fuse:
	${AVRDUDE_CMD_BASE} -p ${MCU_NAME_AVRDUDE} \
	                    -U lfuse:w:0xe1:m


#----------------------------
# -- Common                --
#----------------------------

clean:
	rm -f $(OBJECTS) $(LISTINGS) \
	      ${PROG_NAME}.hex ${PROG_NAME}.elf \
		  ${PROG_NAME}.map ${PROG_NAME}.disasm \
		  $(TEST_OBJECTS) ${PROG_NAME}.avr-size-report.txt \
		  ${PROG_NAME}.test-report.txt \
		  Coverage.xml Coverage.html Coverage.debouncing.c.html \
		  Coverage.txt
	rm -rf build/tests
	rm -rf build/firmware

# https://stackoverflow.com/a/45048948
$(info $(shell mkdir -p $(DIRS)))



