#include <stdint.h>

#include "button.h"
#include "debouncing.h"

inline void configureButtons(PinState* button1)
{
    // configuring button pin for input
    BUTTON1_DIRECTION_REGISTER &= ~BUTTON1_PIN; 
    // disabling button pin built-in pullup
    BUTTON1_DATA_REGISTER &= ~BUTTON1_PIN; 

    UpdatePinState(
        button1,
        button1IsUp(),
        0);
    MarkStateAsProcessed(button1);
}

inline void updateButtonsState(
    PinState* button1, 
    uint32_t currentTime)
{    
    UpdatePinState(
        button1,
        button1IsUp(),
        currentTime);
}

inline int8_t getButton1Event(
    PinState* button1, 
    uint32_t currentTime)
{
    int8_t button1Event =
        GetUnprocessedEvent(
            button1,
            DEBOUNCING_THRESHOLD_MS,
            LONG_BUTTON_PRESS_DURATION_MS,
            currentTime);
    if(button1Event!=NO_UNPROCESSED_EVENTS)
    {
        MarkStateAsProcessed(button1);
    }
    return button1Event;
}
