#include "debouncing.h"

#define _NO_LONG_PRESS            0
#define _LONG_PRESS_NOT_PROCESSED 1
#define _LONG_PRESS_PROCESSED     2

void UpdatePinState(
  PinState* pinState,
  uint8_t newValue,
  uint32_t currentTime)
{
  if(pinState->StateCurrent != newValue){
      pinState->StateCurrent = newValue;
      pinState->lastChangeTime = currentTime;
  }
}

int8_t GetUnprocessedEvent(
  PinState* pinState,
  uint32_t debouncingThreshold,
  uint32_t longPressDuration,
  uint32_t currentTime)
{
  uint32_t timeSinceLastChange =
    currentTime - pinState->lastChangeTime;
  if(timeSinceLastChange >= debouncingThreshold){
    if(pinState->StateOld){
      if(!pinState->StateCurrent){
        // was UP and now DOWN
        pinState->longPressStatus = _NO_LONG_PRESS;
        pinState->StateOld = pinState->StateCurrent;
      }
    }
    else{
      if(pinState->StateCurrent){
        // was DOWN and now UP
        if(pinState->longPressStatus){
          // if long button press was registered 
          // previously, short button press
          // is not registered on button up
          pinState->StateOld = pinState->StateCurrent;
        } else {
          // Button released after debouncingThreshold
          // but before longPressDuration.
          // registering short 
          return SHORT_BUTTON_PRESS;
        }
      } else {
        // was DOWN and now DOWN
        if((timeSinceLastChange >= longPressDuration) &&
            (pinState->longPressStatus !=
                _LONG_PRESS_PROCESSED)) {
          // Returning long press event if button 
          // has been pressed for longer than 
          // longPressDuration.
          //
          // Long press is registered while
          // button is still pressed as opposed 
          // to short press
          pinState->longPressStatus =
                _LONG_PRESS_NOT_PROCESSED;
          return LONG_BUTTON_PRESS;
        }
      }
    }
  }

  return NO_UNPROCESSED_EVENTS;
}

void MarkStateAsProcessed(PinState* pinState)
{
    pinState->StateOld = pinState->StateCurrent;
    // Only "forgetting" about long press after
    // UP edge (long or short) so that
    // longPress is not reset when user code
    // marks long press as processed
    if(pinState->longPressStatus==_LONG_PRESS_NOT_PROCESSED) {
      pinState->longPressStatus = _LONG_PRESS_PROCESSED;
    }
}