#ifndef __settings_h_guard__
#define __settings_h_guard__

#include <avr/eeprom.h>
#include <stdint.h>

#define EEPROM_ADDR_VERSION               (uint8_t*)0
#define EEPROM_ADDR_MIN_DELAY             (uint8_t*)1
#define EEPROM_ADDR_DELAY_INTERVAL_LENGTH (uint8_t*)2

#define EEPROM_VERSION 2
#define DEFAULT_DELAY_MIN_DELAY       2
#define DEFAULT_DELAY_INTERVAL_LENGTH 4

typedef struct SelfMovingMouseSettings {
    // Minimum delay length measured in
    // DELAY_BOUNDARY_INCREMENT_MS increments
    uint8_t minDelay;
    // Delay interval length measured in
    // DELAY_BOUNDARY_INCREMENT_MS increments
    uint8_t delayIntervalLength;
} SelfMovingMouseSettings;


static inline void restoreSettingsFromEEPROM(
    SelfMovingMouseSettings* settings) {

  uint8_t currentVersion = 
    eeprom_read_byte(EEPROM_ADDR_VERSION);
  if(currentVersion == EEPROM_VERSION)
  {
    // Reading whole block at once 
    // like so increases code size by 28 bytes
    //eeprom_read_block(
    //    settings, 
    //    EEPROM_ADDR_MIN_DELAY, 
    //    sizeof(SelfMovingMouseSettings));

    settings->minDelay = eeprom_read_byte(EEPROM_ADDR_MIN_DELAY);
    settings->delayIntervalLength =
      eeprom_read_byte(EEPROM_ADDR_DELAY_INTERVAL_LENGTH);
  }
  else
  {
    // EEPROM version differs
    // using default values
    settings->minDelay = DEFAULT_DELAY_MIN_DELAY;
    settings->delayIntervalLength = DEFAULT_DELAY_INTERVAL_LENGTH;

    // EEPROM not updated here
    // to reduce code size and to conserve
    // write cycles resource.
    // Wrong version at EEPROM_ADDR_VERSION
    // has the same effect as default values.
  }
}

static inline void saveSettingsToEEPROM(
    const SelfMovingMouseSettings* settings){
  // eeprom_update_byte(..) only updates EEPROM
  // if current value does not match
  // new value to converve write cycles resource
  eeprom_update_byte(
    EEPROM_ADDR_VERSION,
    EEPROM_VERSION);
  eeprom_update_byte(
    EEPROM_ADDR_MIN_DELAY,
    settings->minDelay);
  eeprom_update_byte(
    EEPROM_ADDR_DELAY_INTERVAL_LENGTH,
    settings->delayIntervalLength);
}

#endif