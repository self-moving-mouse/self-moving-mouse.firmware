#ifndef HEADER_GUARD_TRACK210
#define HEADER_GUARD_TRACK210

#define TRACK210_TSTEP_MS 8

// first two bytes contain x and y components of initial velocity
// in pixels/step (should be cast to int8 type)
// all the following bytes contain 2 4-bit codes
// of x acceleration (low bits) and y acceleration (high bits) 
// in pixels/step^2 
const PROGMEM uint8_t TRACK210_DDPCM[173] = {
    0x04,
    0x00,
    0x7b,
    0x8b,
    0x7b,
    0x9b,
    0x7b,
    0x78,
    0x98,
    0x79,
    0x76,
    0xb6,
    0x74,
    0x83,
    0x76,
    0x93,
    0x74,
    0x97,
    0xa4,
    0x95,
    0xb2,
    0xc3,
    0x85,
    0x82,
    0x6b,
    0x68,
    0x6b,
    0x4b,
    0x5d,
    0x5c,
    0x6b,
    0x8b,
    0xae,
    0x9d,
    0x9e,
    0x6d,
    0x7b,
    0x77,
    0x83,
    0x74,
    0x71,
    0x70,
    0x53,
    0x75,
    0xb3,
    0x84,
    0xb1,
    0x93,
    0x78,
    0x78,
    0x99,
    0x5d,
    0x6d,
    0x5d,
    0x9d,
    0xbd,
    0x7e,
    0x7c,
    0x9b,
    0x59,
    0x79,
    0xb6,
    0x52,
    0x61,
    0x71,
    0x76,
    0x72,
    0x97,
    0x85,
    0xa2,
    0xa1,
    0x77,
    0x75,
    0x87,
    0x5b,
    0x6b,
    0x7e,
    0x6d,
    0x6a,
    0x8b,
    0x7c,
    0x8c,
    0x6e,
    0x78,
    0x97,
    0x76,
    0x73,
    0x91,
    0x73,
    0x77,
    0x96,
    0x75,
    0x73,
    0x92,
    0x72,
    0x73,
    0x91,
    0x7d,
    0x7b,
    0x9d,
    0x7e,
    0x7c,
    0x9d,
    0x7b,
    0x6e,
    0x6d,
    0x6e,
    0x7a,
    0x73,
    0xa4,
    0x81,
    0x81,
    0x93,
    0x74,
    0x72,
    0x41,
    0x71,
    0x40,
    0x96,
    0x77,
    0x97,
    0x7d,
    0x6d,
    0x9e,
    0xbf,
    0x9d,
    0x7e,
    0x4c,
    0x5f,
    0x6c,
    0x7d,
    0x6b,
    0x84,
    0x91,
    0x91,
    0xa2,
    0xa1,
    0x83,
    0x52,
    0x72,
    0x50,
    0x95,
    0x97,
    0x7c,
    0xba,
    0x7d,
    0x9e,
    0x8c,
    0x7d,
    0x79,
    0x6d,
    0x4e,
    0x3a,
    0xba,
    0x84,
    0x4b,
    0xc0,
    0xb2,
    0xb3,
    0x74,
    0x61,
    0x72,
    0x93,
    0x97,
    0x7c,
    0x9b,
    0x7e,
    0xbd,
    0x6b,
    0x74,
    0x99,
    0x79,
    0x77,
};

#define TRACK210_LENGTH (sizeof(TRACK210_DDPCM)-2)

#endif
