#ifndef HEADER_GUARD_TRACK191
#define HEADER_GUARD_TRACK191

#define TRACK191_TSTEP_MS 8

// first two bytes contain x and y components of initial velocity
// in pixels/step (should be cast to int8 type)
// all the following bytes contain 2 4-bit codes
// of x acceleration (low bits) and y acceleration (high bits) 
// in pixels/step^2 
const PROGMEM uint8_t TRACK191_DDPCM[110] = {
    0x01,
    0x01,
    0x98,
    0x9a,
    0x87,
    0x9a,
    0x77,
    0x7b,
    0x77,
    0x68,
    0x69,
    0x88,
    0x6a,
    0x47,
    0x7a,
    0x77,
    0x75,
    0x78,
    0x73,
    0x99,
    0x55,
    0x76,
    0x77,
    0x75,
    0x76,
    0x85,
    0x75,
    0x87,
    0xa8,
    0xb6,
    0x88,
    0xa8,
    0x77,
    0xa7,
    0x96,
    0x85,
    0x97,
    0xb7,
    0xb6,
    0xb7,
    0x76,
    0x97,
    0x77,
    0x7a,
    0x78,
    0x54,
    0x49,
    0x77,
    0x77,
    0x5c,
    0x67,
    0x7b,
    0x57,
    0x7a,
    0x77,
    0x37,
    0x85,
    0x4a,
    0x87,
    0x6b,
    0x6a,
    0x78,
    0x7a,
    0xa8,
    0xb9,
    0xaa,
    0x99,
    0xa9,
    0x8c,
    0xac,
    0xbd,
    0xcc,
    0x9b,
    0xb7,
    0x79,
    0xb6,
    0x59,
    0x64,
    0x64,
    0x36,
    0x67,
    0x67,
    0x37,
    0x47,
    0x64,
    0x63,
    0x53,
    0x77,
    0x74,
    0x47,
    0x23,
    0xb4,
    0x84,
    0x94,
    0xb4,
    0xb4,
    0xb6,
    0xb6,
    0xb6,
    0xb9,
    0x9b,
    0x9b,
    0x7a,
    0x8b,
    0x6c,
    0x78,
    0x89,
    0x69,
    0x77,
    0x7a,
};

#define TRACK191_LENGTH (sizeof(TRACK191_DDPCM)-2)

#endif
