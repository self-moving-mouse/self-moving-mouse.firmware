#ifndef HEADER_GUARD_TRACK155
#define HEADER_GUARD_TRACK155

#define TRACK155_TSTEP_MS 8

// first two bytes contain x and y components of initial velocity
// in pixels/step (should be cast to int8 type)
// all the following bytes contain 2 4-bit codes
// of x acceleration (low bits) and y acceleration (high bits) 
// in pixels/step^2 
const PROGMEM uint8_t TRACK155_DDPCM[115] = {
    0xff,
    0x01,
    0x77,
    0x99,
    0x76,
    0x77,
    0x77,
    0x97,
    0x68,
    0x57,
    0x77,
    0x77,
    0x6b,
    0x57,
    0x8b,
    0x7c,
    0x89,
    0x8c,
    0x9a,
    0xa9,
    0xb6,
    0xa3,
    0x97,
    0x94,
    0x74,
    0xa5,
    0x76,
    0x85,
    0x85,
    0xa4,
    0xa5,
    0x47,
    0x5a,
    0x49,
    0x5b,
    0x6c,
    0x79,
    0x97,
    0x4a,
    0x6a,
    0x4b,
    0x6b,
    0xb7,
    0x97,
    0x98,
    0x94,
    0x96,
    0x94,
    0x97,
    0x76,
    0xa4,
    0x75,
    0xa7,
    0x78,
    0x47,
    0x77,
    0x77,
    0x69,
    0x59,
    0x79,
    0x78,
    0x7a,
    0x69,
    0x7b,
    0x5d,
    0x7d,
    0xc9,
    0xa7,
    0xb2,
    0x84,
    0x93,
    0x76,
    0xb5,
    0x85,
    0x76,
    0xa7,
    0x85,
    0x64,
    0x65,
    0x66,
    0x67,
    0x4a,
    0x5b,
    0x8d,
    0x5a,
    0x7b,
    0x5c,
    0x2e,
    0xcb,
    0xa9,
    0xb7,
    0xb7,
    0x73,
    0xc2,
    0x74,
    0x94,
    0x96,
    0x75,
    0xa4,
    0x83,
    0x74,
    0x75,
    0x57,
    0x69,
    0x7a,
    0x7c,
    0x6b,
    0x7c,
    0x88,
    0x4b,
    0x59,
    0x79,
    0x96,
    0xa6,
    0x86,
};

#define TRACK155_LENGTH (sizeof(TRACK155_DDPCM)-2)

#endif
