#ifndef HEADER_GUARD_TRACK195
#define HEADER_GUARD_TRACK195

#define TRACK195_TSTEP_MS 8

// first two bytes contain x and y components of initial velocity
// in pixels/step (should be cast to int8 type)
// all the following bytes contain 2 4-bit codes
// of x acceleration (low bits) and y acceleration (high bits) 
// in pixels/step^2 
const PROGMEM uint8_t TRACK195_DDPCM[111] = {
    0x01,
    0x01,
    0x88,
    0x9a,
    0x77,
    0x8b,
    0x78,
    0x79,
    0x87,
    0x69,
    0x76,
    0x78,
    0x9b,
    0x64,
    0x76,
    0x76,
    0x86,
    0x76,
    0x97,
    0x69,
    0x76,
    0x87,
    0x77,
    0x87,
    0x79,
    0x77,
    0x77,
    0x79,
    0x97,
    0x76,
    0x88,
    0x77,
    0xb8,
    0x76,
    0xa8,
    0x77,
    0x97,
    0x78,
    0xa6,
    0x87,
    0x87,
    0xc7,
    0x97,
    0xb5,
    0x86,
    0xb6,
    0x66,
    0x49,
    0x67,
    0x4b,
    0x49,
    0x77,
    0x77,
    0x57,
    0x79,
    0x76,
    0x58,
    0x78,
    0x78,
    0x78,
    0x69,
    0x79,
    0x87,
    0x6b,
    0x77,
    0x9b,
    0x78,
    0x7c,
    0xaa,
    0x99,
    0x9b,
    0x8b,
    0xcb,
    0xba,
    0xb8,
    0x96,
    0x96,
    0x74,
    0x97,
    0x77,
    0x67,
    0x66,
    0x36,
    0x74,
    0x77,
    0x67,
    0x78,
    0x76,
    0x78,
    0x65,
    0x67,
    0x69,
    0x75,
    0x37,
    0x47,
    0x66,
    0x94,
    0x76,
    0xb9,
    0xa7,
    0x86,
    0xb5,
    0xa9,
    0xa8,
    0x98,
    0x78,
    0x98,
    0x88,
    0x79,
    0x88,
    0x78,
};

#define TRACK195_LENGTH (sizeof(TRACK195_DDPCM)-2)

#endif
