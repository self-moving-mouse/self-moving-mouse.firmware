#ifndef HEADER_GUARD_TRACK190
#define HEADER_GUARD_TRACK190

#define TRACK190_TSTEP_MS 8

// first two bytes contain x and y components of initial velocity
// in pixels/step (should be cast to int8 type)
// all the following bytes contain 2 4-bit codes
// of x acceleration (low bits) and y acceleration (high bits) 
// in pixels/step^2 
const PROGMEM uint8_t TRACK190_DDPCM[81] = {
    0x02,
    0x00,
    0x77,
    0x7a,
    0x78,
    0x7b,
    0x87,
    0x78,
    0x77,
    0x8b,
    0x75,
    0x87,
    0x79,
    0x85,
    0x85,
    0x95,
    0x87,
    0x77,
    0x99,
    0x76,
    0xa5,
    0x93,
    0x84,
    0x93,
    0x97,
    0x67,
    0x7b,
    0x5d,
    0x5d,
    0x78,
    0x7b,
    0x8c,
    0x3e,
    0x8d,
    0x5b,
    0x66,
    0xb1,
    0x91,
    0x94,
    0x84,
    0x86,
    0x87,
    0x94,
    0x86,
    0x84,
    0x84,
    0x74,
    0x9b,
    0x4d,
    0x6d,
    0x79,
    0x87,
    0x7a,
    0x7b,
    0x6e,
    0x7c,
    0x78,
    0x92,
    0x81,
    0x95,
    0x77,
    0xa5,
    0x76,
    0x93,
    0x95,
    0xa3,
    0x75,
    0x96,
    0x5b,
    0x7d,
    0x5e,
    0x56,
    0x89,
    0x87,
    0x7a,
    0x7b,
    0x89,
    0x59,
    0x78,
    0x94,
    0x84,
};

#define TRACK190_LENGTH (sizeof(TRACK190_DDPCM)-2)

#endif
