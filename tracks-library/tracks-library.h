#ifndef HEADER_GUARD_TRACKS_LIBRARY
#define HEADER_GUARD_TRACKS_LIBRARY

#include "track061.h"
#include "track070.h"
#include "track071.h"
#include "track073.h"
#include "track151.h"
#include "track153.h"
#include "track155.h"
#include "track156.h"
#include "track157.h"
#include "track158.h"
#include "track177.h"
#include "track178.h"
#include "track179.h"
#include "track180.h"
#include "track186.h"
#include "track190.h"
#include "track191.h"
#include "track192.h"
#include "track194.h"
#include "track195.h"
#include "track196.h"
#include "track197.h"
#include "track198.h"
#include "track199.h"
#include "track200.h"
#include "track201.h"
#include "track206.h"
#include "track209.h"
#include "track210.h"
#include "track213.h"
#include "track216.h"
#include "track221.h"
#include "track223.h"
#include "track224.h"
#include "track225.h"
#include "track226.h"
#include "track227.h"
#include "track229.h"

static PGM_VOID_P tracks[] = {
    &TRACK061_DDPCM,
    &TRACK070_DDPCM,
    &TRACK071_DDPCM,
    &TRACK073_DDPCM,
    &TRACK151_DDPCM,
    &TRACK153_DDPCM,
    &TRACK155_DDPCM,
    &TRACK156_DDPCM,
    &TRACK157_DDPCM,
    &TRACK158_DDPCM,
    &TRACK177_DDPCM,
    &TRACK178_DDPCM,
    &TRACK179_DDPCM,
    &TRACK180_DDPCM,
    &TRACK186_DDPCM,
    &TRACK190_DDPCM,
    &TRACK191_DDPCM,
    &TRACK192_DDPCM,
    &TRACK194_DDPCM,
    &TRACK195_DDPCM,
    &TRACK196_DDPCM,
    &TRACK197_DDPCM,
    &TRACK198_DDPCM,
    &TRACK199_DDPCM,
    &TRACK200_DDPCM,
    &TRACK201_DDPCM,
    &TRACK206_DDPCM,
    &TRACK209_DDPCM,
    &TRACK210_DDPCM,
    &TRACK213_DDPCM,
    &TRACK216_DDPCM,
    &TRACK221_DDPCM,
    &TRACK223_DDPCM,
    &TRACK224_DDPCM,
    &TRACK225_DDPCM,
    &TRACK226_DDPCM,
    &TRACK227_DDPCM,
    &TRACK229_DDPCM,
    };

static const uint8_t trackSizes[] = {
    TRACK061_LENGTH,
    TRACK070_LENGTH,
    TRACK071_LENGTH,
    TRACK073_LENGTH,
    TRACK151_LENGTH,
    TRACK153_LENGTH,
    TRACK155_LENGTH,
    TRACK156_LENGTH,
    TRACK157_LENGTH,
    TRACK158_LENGTH,
    TRACK177_LENGTH,
    TRACK178_LENGTH,
    TRACK179_LENGTH,
    TRACK180_LENGTH,
    TRACK186_LENGTH,
    TRACK190_LENGTH,
    TRACK191_LENGTH,
    TRACK192_LENGTH,
    TRACK194_LENGTH,
    TRACK195_LENGTH,
    TRACK196_LENGTH,
    TRACK197_LENGTH,
    TRACK198_LENGTH,
    TRACK199_LENGTH,
    TRACK200_LENGTH,
    TRACK201_LENGTH,
    TRACK206_LENGTH,
    TRACK209_LENGTH,
    TRACK210_LENGTH,
    TRACK213_LENGTH,
    TRACK216_LENGTH,
    TRACK221_LENGTH,
    TRACK223_LENGTH,
    TRACK224_LENGTH,
    TRACK225_LENGTH,
    TRACK226_LENGTH,
    TRACK227_LENGTH,
    TRACK229_LENGTH,
  };

#define N_TRACKS (sizeof(trackSizes))  

#endif
