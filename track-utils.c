
#include <avr/pgmspace.h>

#include "mouse.h"
#include "ddpcm_difftable.h"
#include "track-utils.h"

inline void setTrack(
    TrackPlayerState* state, 
    PGM_VOID_P track, 
    uint8_t trackLength)
{
  state->track = track;
  state->trackSize = trackLength; 
  state->currentDX = 
    (int8_t)pgm_read_byte(state->track);
  state->currentDY = 
    (int8_t)pgm_read_byte(state->track + 1);
  state->currentStep = 0;
}

// Plays next track step (if any).
// Returns false if end of track is reached
inline uint8_t playNextStep(TrackPlayerState* state)
{    
  setDxDy(state->currentDX, state->currentDY);

  if((state->currentStep) >= (state->trackSize)){
    return 0;
  }
  
  uint8_t accelerationXYIndexes =
    pgm_read_byte(state->track + state->currentStep+2);

  uint8_t accelerationXIdx = accelerationXYIndexes & 0x0F;    
  state->currentDX += 
    (int8_t)pgm_read_byte(DDPCM_DELTA+accelerationXIdx);

  uint8_t acceleration_y_idx = (accelerationXYIndexes & 0xF0) >> 4;    
  state->currentDY += 
    (int8_t)pgm_read_byte(DDPCM_DELTA+acceleration_y_idx);
            
  state->currentStep++;
  return 1;
}
