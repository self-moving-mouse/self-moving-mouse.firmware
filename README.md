This repository contains firmware source code for Self-Moving Mouse. 

## Self-Moving Mouse

Self-Moving Mouse is user presence emulator with a custom PCB 
that is basically a clone of [Digispark](http://digistump.com/products/1) 
board with additional button and a form-factor more suitable 
for USB dongle case. 

It pretends to be a USB-HID mouse and plays mouse tracks
stored in it's memory in random order with random delays to
prevent host system from going to sleep. 
It was created mostly just for fun but can be useful 
in some unusual circumstances when software
solutions of similar nature can not be used for some reason.

See [this repo](https://gitlab.com/self-moving-mouse/self-moving-mouse.hardware) 
for board and case design files.

The firmware uses 
[V-USB](https://www.obdev.at/products/vusb/index.html) 
library for purely software USB emulation on ATTiny85 MCU 
that does not have USB peripheral.

Mouse tracks compressed with DPCM-like algorithm are stored 
in program memory of the MCU (see `tracks-library`, 
`track-utils.c`, `track-utils.h`).

See `main-states.plantuml`/`main-states.svg` for internal state
machine diagram.

Documentation for rev1.0 with LaTeX sources is available 
[here](https://gitlab.com/fedorkotov/self-moving-mouse.documentation) 
(russian version only).

Auxiliary scripts and tools including mouse tracks recorder and 
encoders are in 
[this](https://gitlab.com/fedorkotov/self-moving-mouse.tools) repo.

## Legal Info

Source code in this repository is 
licensed under [GNU GPL v2](https://opensource.org/licenses/GPL-2.0) license. 
I would prefer something more permissive if I could but
V-USB library I use is under viral GPL v2 so I have to use GPL too.
