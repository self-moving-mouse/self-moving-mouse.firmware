#include <avr/io.h>
#include <avr/interrupt.h>

#include "timeconfig.h"
#include "time.h"

void configureTimer(ClockState* clockState)
{
    clockState->milliseconds = 0L;
    #if CYCLES_EXCESS > 0
    clockState->remainderCycles = 0L;
    #endif

    // configuring timer 0 to use waveform generation mode
    // 010 (CTC, count to OCRA and reset)
    TCCR0A |= (1<< WGM01);

    // timer interrupt every 
    // TIMER_MAX_CTR ticks.
    OCR0A = TIMER_MAX_CTR;

    #if TIMER_DIVIDER == 1024
        TCCR0B |= ((1<<CS00) | (1<<CS02));
    #elif TIMER_DIVIDER == 256
        TCCR0B |= (1<<CS02);
    #elif TIMER_DIVIDER == 64
        TCCR0B |= ((1<<CS00) | (1<<CS01));
    #elif TIMER_DIVIDER == 8
        TCCR0B |= (1<<CS01);
    #elif TIMER_DIVIDER == 1
        TCCR0B |= (1<<CS00);
    #else
        #error Unsupported TIMER_DIVIDER value in time module
    #endif

    // Enable timer0 OCR0A compare match interrupt
    TIMSK |= (1 << OCIE0A);
}

void updateTimer(ClockState* clockState)
{
    #if CYCLES_EXCESS == 0
        clockState->milliseconds += WHOLE_MILLISECONDS_IN_TIMER_TICK;
    #else
        // Creating local copy of volatile
        // remainderCycles so that the compiler
        // can store it in registers (each access to a volatile 
        // variable has to use memory directly.
        // This also reduces code size.            
        uint16_t remainderCycles = clockState->remainderCycles;
        remainderCycles += CYCLES_EXCESS;
        clockState->remainderCycles = remainderCycles;
        if(remainderCycles >= CLOCK_CYCLES_IN_MS) {
            remainderCycles -= CLOCK_CYCLES_IN_MS;
        } else if (remainderCycles>=CLOCK_CYCLES_IN_HALF_MS &&
                   remainderCycles<CLOCK_CYCLES_IN_HALF_MS_PLUS_ONE_TICK) {

            clockState->milliseconds  += (WHOLE_MILLISECONDS_IN_TIMER_TICK + 1);
            return;
        }
        clockState->milliseconds  += WHOLE_MILLISECONDS_IN_TIMER_TICK;
    #endif
}


uint32_t millis(ClockState* clockState)
{
    cli();
    uint32_t buf = clockState->milliseconds;
    sei();
    return buf;
}