#ifndef CLOCK_HEADER_GUARD
#define CLOCK_HEADER_GUARD

#include <stdint.h>

// Assumes CLOCK_STEP_MS is defined in "timeconfig.h"
#include "timeconfig.h"

#define CLOCK_CYCLES_IN_MS   (F_CPU / 1000L)

#define CLOCK_TIMER_INTERRUPT_VECT TIMER0_COMPA_vect

// This parameters were selected with
// a python script in Util/ClockParametersSelection.ipynb

#if F_CPU == 20000000
    #if CLOCK_STEP_MS == 1
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 79L
    #elif CLOCK_STEP_MS == 2
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 157L
    #elif CLOCK_STEP_MS == 4
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 79L
    #elif CLOCK_STEP_MS == 5
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 98L
    #elif CLOCK_STEP_MS == 8
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 157L
    #elif CLOCK_STEP_MS == 10
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 196L
    #else
        #error time module does not support specified CLOCK_STEP_MS for this F_CPU
    #endif
#elif F_CPU == 16500000
    #if CLOCK_STEP_MS == 1
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 65L
    #elif CLOCK_STEP_MS == 2
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 129L
    #elif CLOCK_STEP_MS == 4
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 65L
    #elif CLOCK_STEP_MS == 5
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 81L
    #elif CLOCK_STEP_MS == 8
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 129L
    #elif CLOCK_STEP_MS == 10
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 162L
    #elif CLOCK_STEP_MS == 15
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 242L
    #else
        #error time module does not support specified CLOCK_STEP_MS for this F_CPU
    #endif
#elif F_CPU == 16000000
    #if CLOCK_STEP_MS == 1
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 250L
    #elif CLOCK_STEP_MS == 2
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 4
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 250L
    #elif CLOCK_STEP_MS == 5
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 79L
    #elif CLOCK_STEP_MS == 8
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 10
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 157L
    #elif CLOCK_STEP_MS == 15
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 235L
    #else
        #error time module does not support specified CLOCK_STEP_MS for this F_CPU
    #endif
#elif F_CPU == 8000000
    #if CLOCK_STEP_MS == 1
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 2
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 250L
    #elif CLOCK_STEP_MS == 4
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 5
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 157L
    #elif CLOCK_STEP_MS == 8
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 250L
    #elif CLOCK_STEP_MS == 10
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 79L
    #elif CLOCK_STEP_MS == 15
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 118L
    #elif CLOCK_STEP_MS == 20
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 157L
    #else
        #error time module does not support specified CLOCK_STEP_MS for this F_CPU
    #endif
#elif F_CPU == 4000000
    #if CLOCK_STEP_MS == 1
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 63L
    #elif CLOCK_STEP_MS == 2
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 4
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 250L
    #elif CLOCK_STEP_MS == 5
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 79L
    #elif CLOCK_STEP_MS == 8
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 10
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 157L
    #elif CLOCK_STEP_MS == 15
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 235L
    #elif CLOCK_STEP_MS == 20
        #define TIMER_DIVIDER 1024L
        #define TIMER_MAX_CTR 79L
    #else
        #error time module does not support specified CLOCK_STEP_MS for this F_CPU
    #endif
#elif F_CPU == 2000000
    #if CLOCK_STEP_MS == 1
        #define TIMER_DIVIDER 8L
        #define TIMER_MAX_CTR 250L
    #elif CLOCK_STEP_MS == 2
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 63L
    #elif CLOCK_STEP_MS == 4
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 5
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 157L
    #elif CLOCK_STEP_MS == 8
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 250L
    #elif CLOCK_STEP_MS == 10
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 79L
    #elif CLOCK_STEP_MS == 15
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 118L
    #elif CLOCK_STEP_MS == 20
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 157L
    #else
        #error time module does not support specified CLOCK_STEP_MS for this F_CPU
    #endif
#elif F_CPU == 1000000
    #if CLOCK_STEP_MS == 1
        #define TIMER_DIVIDER 8L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 2
        #define TIMER_DIVIDER 8L
        #define TIMER_MAX_CTR 250L
    #elif CLOCK_STEP_MS == 4
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 63L
    #elif CLOCK_STEP_MS == 5
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 79L
    #elif CLOCK_STEP_MS == 8
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 125L
    #elif CLOCK_STEP_MS == 10
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 157L
    #elif CLOCK_STEP_MS == 15
        #define TIMER_DIVIDER 64L
        #define TIMER_MAX_CTR 235L
    #elif CLOCK_STEP_MS == 20
        #define TIMER_DIVIDER 256L
        #define TIMER_MAX_CTR 79L
    #else
        #error time module does not support specified CLOCK_STEP_MS for this F_CPU
    #endif
#else
    #error time module does not support F_CPU value provided
#endif

#define CYCLES_IN_TIMER_TICK (TIMER_DIVIDER * TIMER_MAX_CTR)
#define WHOLE_MILLISECONDS_IN_TIMER_TICK (CYCLES_IN_TIMER_TICK / CLOCK_CYCLES_IN_MS)
#define CYCLES_EXCESS (CYCLES_IN_TIMER_TICK % CLOCK_CYCLES_IN_MS)

#if CYCLES_EXCESS > 0
    #define CLOCK_CYCLES_IN_HALF_MS (CLOCK_CYCLES_IN_MS / 2) 
    #define CLOCK_CYCLES_IN_HALF_MS_PLUS_ONE_TICK (CLOCK_CYCLES_IN_HALF_MS + CYCLES_EXCESS)
#endif

typedef struct ClockState {
    volatile uint32_t milliseconds;
    #if CYCLES_EXCESS > 0
    volatile uint16_t remainderCycles;
    #endif
} ClockState;



// This method should be called before millis()
// or getMillis(..).
// Configures timer 0 and it's interrupt.
// The timer can not be used for anything else.
// Does not enable interrupts (sei()).
// The timer will start counting when 
// sei() is called for the first time.
void configureTimer(ClockState* clockState);

// Should be called from CLOCK_TIMER_INTERRUPT_VECT
// interrupt handler. 
// Interrupts happen approximately 
// rather infrequently (
// every CLOCK_STEP_MS milliseconds)
// so interrupt handler can do something else
// but it is recommended to avoid it if possible.
void updateTimer(ClockState* clockState);

// Gets current time value (milliseconds since 
// first sei() call after configureTimer()). 
// The value is approximate.
// It can not decrease but can be 
// from TRUE_TIME - WHOLE_MILLISECONDS_IN_TIMER_TICK - 0.5 ms 
// to   TRUE_TIME + 0.5 ms
uint32_t millis(ClockState* clockState);
#endif
