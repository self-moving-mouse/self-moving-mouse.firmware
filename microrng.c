#include <stdint.h>

#include "microrng.h"

#ifndef TEST
#include <avr/io.h>
#include <avr/interrupt.h>
#endif

inline void configureRNG()
{
    #ifndef TEST
    MCUSR = 0;
  
    /* Start timed sequence */
    WDTCR |= (1<<WDCE | 1<<WDE);

    /* Put WDT into interrupt mode */
    /* Set shortest prescaler(time-out) value = 2048 cycles (~16 ms) */
    WDTCR = 1<<WDIE;
    #endif
}

inline void RNGOnWatchdog(
  RNGState* rngState, 
  uint8_t sample)
{
    if(!rngState->sample_waiting){
        rngState->sample = sample;
        rngState->sample_waiting = 1;
    }
}

// Rotate bits to the left
// https://en.wikipedia.org/wiki/Circular_shift
static inline uint8_t _rotl_1(const uint8_t value) 
{    
  return (value << 1) | (value >> 7);
}

inline uint8_t updateRandomSample(RNGState* rngState)
{
  if(rngState->current_bit>7){
    return 1;
  }

  if(rngState->sample_waiting){
    rngState->sample_waiting = 0;

    // Spreading randomness around
    rngState->random_number = 
      _rotl_1(rngState->random_number);
    // XOR preserves randomness
    rngState->random_number ^= rngState->sample;

    rngState->current_bit++;
    return 0;
  }

  return 1;
}

inline uint8_t useRandomNumber(RNGState* rngState)
{
    uint8_t result = rngState->random_number;
    rngState->current_bit = 0;
    return result;
}
