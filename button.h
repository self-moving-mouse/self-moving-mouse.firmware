#ifndef __button_h_guard__
#define __button_h_guard__

#include <avr/io.h>
#include <avr/interrupt.h>

#include "debouncing.h"

#define DEBOUNCING_THRESHOLD_MS       20L
#define LONG_BUTTON_PRESS_DURATION_MS 2000L

#define BUTTON1_PIN                (1 << PB1)
#define BUTTON1_DATA_REGISTER      PORTB
#define BUTTON1_INPUT_REGISTER     PINB
#define BUTTON1_DIRECTION_REGISTER DDRB

#define button1IsUp() (BUTTON1_INPUT_REGISTER & BUTTON1_PIN)

// Configures button pin and initializes 
// PinState structure.
void configureButtons(PinState* button1);

// Detects pin state changes and updates button state.
//
// Preferably should be called from button pin 
// interrupt handler or on each main loop iteration.
void updateButtonsState(
    PinState* button1,
    uint32_t currentTime);

// Gets button event (if any) and marks it as processed
// in PinState structure
int8_t getButton1Event(
    PinState* button1, 
    uint32_t currentTime);

#endif // __button_h_guard__
