#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "time.h"

#define STATUS_LED_PIN                (1 << PB0)
#define STATUS_LED_DATA_REGISTER      PORTB
#define STATUS_LED_DIRECTION_REGISTER DDRB

#define configureStatusLED() STATUS_LED_DIRECTION_REGISTER |= STATUS_LED_PIN

#define statusLEDOn()  STATUS_LED_DATA_REGISTER |= (STATUS_LED_PIN)
#define statusLEDOff() STATUS_LED_DATA_REGISTER &= (~(STATUS_LED_PIN))
#define statusLEDToggle() STATUS_LED_DATA_REGISTER ^= (STATUS_LED_PIN)

#define getStatusLEDState() (STATUS_LED_DATA_REGISTER & (STATUS_LED_PIN))

#define BLINK_INTERVAL_MS 500L
#define BLINK_SERIES_DELAY_MS 5000L
#define BLINK_COUNT 3


static ClockState clockState;

ISR(CLOCK_TIMER_INTERRUPT_VECT)
{
   updateTimer(&clockState);
}

int __attribute__((noreturn)) main(void)
{
    configureStatusLED();
    configureTimer(&clockState);

    uint32_t currentTime;
    uint32_t delayEndTime = 0;
    uint8_t blinkCounter = BLINK_COUNT;
    sei();
    
    while(1){
        currentTime = millis(&clockState);
        if(currentTime>=delayEndTime){       
            if(getStatusLEDState())
            {
                blinkCounter--;                
                statusLEDOff();
            }
            else
            {
                statusLEDOn();
            }                      

            if(blinkCounter==0)
            {
                blinkCounter = BLINK_COUNT;
                delayEndTime = currentTime + BLINK_SERIES_DELAY_MS;
            }      
            else
            {
                delayEndTime = currentTime + BLINK_INTERVAL_MS;
            }                        
        } 
    }
}
