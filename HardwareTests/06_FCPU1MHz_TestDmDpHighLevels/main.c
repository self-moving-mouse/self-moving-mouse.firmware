#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// This firmware is for testing if usb data pin
// voltage limiting zeners work and the device is not
// going to fry anything when attached to USB port.
//
// !!! ATENTION !!!
// THE DEVICE MUST NOT BE PLUGGED INTO USB PORT
// WITH THIS FIRMWARE.
// IT IS ONLY FOR TESTING VOLTAGE LEVELS WITH MULTIMETER

#define STATUS_LED_PIN                (1 << PB0)
#define STATUS_LED_DATA_REGISTER      PORTB
#define STATUS_LED_DIRECTION_REGISTER DDRB

#define StatusLEDOn()  STATUS_LED_DATA_REGISTER |= (STATUS_LED_PIN)
#define StatusLEDOff() STATUS_LED_DATA_REGISTER &= (~(STATUS_LED_PIN))

#define USB_DATA_REGISTER        PORTB
#define USB_INPUT_REGISTER       PINB
#define USB_DIRECTION_REGISTER   DDRB

#define USBP_PIN                (1 << PB3)
#define USBM_PIN                (1 << PB4)

#define USBPINSOn()  USB_DATA_REGISTER |= (USBP_PIN | USBM_PIN)
#define USBPINSOff() USB_DATA_REGISTER &= (~(USBP_PIN | USBM_PIN))

#define USBP_On()  USB_DATA_REGISTER |= (USBP_PIN)
#define USBP_Off() USB_DATA_REGISTER &= (~(USBP_PIN))

#define USBM_On()  USB_DATA_REGISTER |= (USBM_PIN)
#define USBM_Off() USB_DATA_REGISTER &= (~(USBM_PIN))

inline void ConfigureIOPins()
{
    // configuring status led pin for output
    STATUS_LED_DIRECTION_REGISTER |= STATUS_LED_PIN;

    // configuring both USB data pins for output
    USB_DIRECTION_REGISTER |= USBM_PIN | USBP_PIN;
}

int __attribute__((noreturn)) main(void)
{
    ConfigureIOPins();
    while(1){
        StatusLEDOn();
        //USBPINSOn();
        USBM_On();
        USBP_Off();
        _delay_ms(5000);
        _delay_ms(5000);
        StatusLEDOff();
        //USBPINSOff();
        USBM_Off();
        USBP_On();
        _delay_ms(5000);
        _delay_ms(5000);
    }
}
