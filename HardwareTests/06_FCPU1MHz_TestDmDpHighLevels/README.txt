Measured D+/D- levels

Board powered from lab power supply attached to 
board GND and USB connector + pin (before polarity protection diode)

LED On
USB D- HIGH 3.4V
USB D+ LOW  0.3V
Supply current 23mA

LED Off
USB D- LOW  0.0V
USB D+ HIGH 3.4V
Supply current 15mA
