#include <avr/io.h>
#include <util/delay.h>

#define STATUS_LED_PIN                (1 << PB0)
#define STATUS_LED_DATA_REGISTER      PORTB
#define STATUS_LED_DIRECTION_REGISTER DDRB

#define StatusLEDOn()  STATUS_LED_DATA_REGISTER |= (STATUS_LED_PIN)
#define StatusLEDOff() STATUS_LED_DATA_REGISTER &= (~(STATUS_LED_PIN))
#define StatusLEDToggle() STATUS_LED_DATA_REGISTER ^= (STATUS_LED_PIN)

int __attribute__((noreturn)) main(void)
{
    // enabling status led pin for output
    STATUS_LED_DIRECTION_REGISTER |= STATUS_LED_PIN;

    while(1){
        StatusLEDOn();
        _delay_ms(1000);
        StatusLEDOff();
        _delay_ms(1000);
    }
}
