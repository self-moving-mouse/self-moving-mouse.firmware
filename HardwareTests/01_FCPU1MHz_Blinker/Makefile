# Based on
# http://arduino.stackexchange.com/questions/12114/basic-makefile-for-avr-gcc
# https://www.avrfreaks.net/forum/makefile-object-not-found-build-v-usb-studio

#----------------
# -- Compiling --
#----------------

CC=avr-gcc
OBJCOPY=avr-objcopy
OBJDUMP=avr-objdump
AVRSIZE=avr-size
PROG_NAME=selfmoving-mouse
MCU_NAME_GCC=attiny85
F_CPU=1000000UL

C_SOURCES = main.c
ASM_SOURCES =

CFLAGS = -Os -std=gnu99 -Wall -I.
CFLAGS += -DF_CPU=${F_CPU}
CFLAGS += -mmcu=$(MCU_NAME_GCC)

COMPILE = $(CC) $(CFLAGS)

LDFLAGS = -Wl,-Map,${PROG_NAME}.map
LDFLAGS += -mmcu=$(MCU_NAME_GCC)

OBJECTS = $(C_SOURCES:.c=.o)
OBJECTS += $(ASM_SOURCES:.S=.o)

LISTINGS = $(C_SOURCES:.c=.lss)


# Generic rule for compiling C files:
%.o: %.c
	$(COMPILE) -c $< -o $@

# Generic rule for assembling Assembler source files:
%.o: %.S
	$(COMPILE) -x assembler-with-cpp -c $< -o $@
# "-x assembler-with-cpp" should not be necessary since this is the default
# file type for the .S (with capital S) extension. However, upper case
# characters are not always preserved on Windows. To ensure WinAVR
# compatibility define the file type manually.

# Generic rule for putputting preprocessing results
%.lss: %.c
	$(COMPILE) -E $< -o $@

build: ${PROG_NAME}.hex

${PROG_NAME}.elf: $(OBJECTS)
	$(CC) $(LDFLAGS) $(LDLIBS) $^ -o $@
	${AVRSIZE} -C --mcu=$(MCU_NAME_GCC) ${PROG_NAME}.elf

${PROG_NAME}.hex: ${PROG_NAME}.elf
	${OBJCOPY} -O ihex -j .text -j .data -R .eeprom $< $@

clean:
	rm -f $(OBJECTS) $(LISTINGS) ${PROG_NAME}.hex ${PROG_NAME}.elf ${PROG_NAME}.map ${PROG_NAME}.disasm

${PROG_NAME}.disasm: ${PROG_NAME}.hex
	$(OBJDUMP) -D -m avr ${PROG_NAME}.hex > ${PROG_NAME}.disasm

disassemble: ${PROG_NAME}.disasm

list: $(LISTINGS)

size: ${PROG_NAME}.elf
	${AVRSIZE} -C --mcu=$(MCU_NAME_GCC) ${PROG_NAME}.elf

debug:
	@echo C_SOURCES=$(C_SOURCES)
	@echo ASM_SOURCES=$(ASM_SOURCES)
	@echo LDFLAGS=$(LDFLAGS)
	@echo LDLIBS=$(LDLIBS)
	@echo OBJECTS=$(OBJECTS)

#----------------
# -- Flashing  --
#----------------
MCU_NAME_AVRDUDE=ATtiny85
AVRDUDE_CMD_BASE=avrdude -c usbasp -b19200  -B10

install: ${PROG_NAME}.hex
	${AVRDUDE_CMD_BASE} -p ${MCU_NAME_AVRDUDE} -U ${PROG_NAME}.hex


