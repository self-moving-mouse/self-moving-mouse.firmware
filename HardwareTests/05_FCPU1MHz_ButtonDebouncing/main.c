#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "time.h"
#include "debouncing.h"

#define STATUS_LED_PIN                (1 << PB0)
#define STATUS_LED_DATA_REGISTER      PORTB
#define STATUS_LED_DIRECTION_REGISTER DDRB

#define StatusLEDOn()  STATUS_LED_DATA_REGISTER |= (STATUS_LED_PIN)
#define StatusLEDOff() STATUS_LED_DATA_REGISTER &= (~(STATUS_LED_PIN))
#define StatusLEDToggle() STATUS_LED_DATA_REGISTER ^= (STATUS_LED_PIN)

#define BUTTON_PIN                (1 << PB1)
#define BUTTON_DATA_REGISTER      PORTB
#define BUTTON_INPUT_REGISTER     PINB
#define BUTTON_DIRECTION_REGISTER DDRB
#define BUTTON_PCINT_PIN          (1 << PCINT1)

#define ButtonIsUp() (BUTTON_INPUT_REGISTER & BUTTON_PIN)

#define BLINK_INTERVAL_MS 250L

#define DEVICE_STATE_OFF   0
#define DEVICE_STATE_BLINK 1
#define DEVICE_STATE_ON    2

/*
State diagram

(*) -> OFF
OFF -(btn short press)-> BLINK
OFF -(btn long press)-> ON
BLINK -(any button press)-> OFF
ON -(any button press)-> OFF
*/

#define DEBOUNCING_THRESHOLD_MS 20L
#define LONG_BUTTON_PRESS_DURATION_MS 2000L
static PinState button1;

inline void ConfigureIOPins()
{
    // configuring status led pin for output
    STATUS_LED_DIRECTION_REGISTER |= STATUS_LED_PIN;

    // configuring button pin for input
    BUTTON_DIRECTION_REGISTER &= ~BUTTON_PIN;
    // disabling button pin built-in pullup
    BUTTON_DATA_REGISTER &= ~BUTTON_PIN;
}

inline void ConfigureButtonInterrupt()
{
    // configure button pin for 
    // pin change interrupt
    PCMSK |= BUTTON_PCINT_PIN;

    // enable pin change interrupt
    GIMSK |= (1 << PCIE);
}

ISR(PCINT0_vect) {    
    uint32_t currentTime;
    getMillis(&currentTime);

    UpdatePinState(
        &button1,
        ButtonIsUp(),
        currentTime);
}

int __attribute__((noreturn)) main(void)
{
    ConfigureIOPins();
    configureTimer();
    ConfigureButtonInterrupt();
    
    UpdatePinState(
        &button1,
        ButtonIsUp(),
        0);
    MarkStateAsProcessed(&button1);

    uint32_t currentTime = 0;
    uint32_t nextBlinkTime = BLINK_INTERVAL_MS;
    uint8_t state = DEVICE_STATE_OFF;

    sei();

    while(1){
        getMillis(&currentTime);

        int8_t button1Event =
            GetUnprocessedEvent(
                &button1,
                DEBOUNCING_THRESHOLD_MS,
                LONG_BUTTON_PRESS_DURATION_MS,
                currentTime);
        if(button1Event!=NO_UNPROCESSED_EVENTS)
        {
            MarkStateAsProcessed(&button1);
        }

        switch (state)
        {
            case DEVICE_STATE_OFF:
                switch(button1Event)
                {
                    case SHORT_BUTTON_PRESS:
                        state = DEVICE_STATE_BLINK;
                        break;
                    case LONG_BUTTON_PRESS:
                        state = DEVICE_STATE_ON;
                        break;
                    default:
                        StatusLEDOff();
                }
                break;
            case DEVICE_STATE_ON:
                switch(button1Event)
                {
                    case NO_UNPROCESSED_EVENTS:
                        StatusLEDOn();
                        break;
                    default:
                        state = DEVICE_STATE_OFF;
                }
                break;
            case DEVICE_STATE_BLINK:
                switch(button1Event)
                {
                    case NO_UNPROCESSED_EVENTS:
                        if(currentTime>=nextBlinkTime){
                            nextBlinkTime = currentTime + BLINK_INTERVAL_MS;
                            StatusLEDToggle();
                        }
                        break;
                    default:
                        state = DEVICE_STATE_OFF;
                }
                break;
        }
    }
}
