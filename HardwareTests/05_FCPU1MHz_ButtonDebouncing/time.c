#include <avr/io.h>
#include <avr/interrupt.h>

#include "timeconfig.h"
#include "time.h"

#define CYCLES_IN_TIMER_TICK (TIMER_DIVIDER * TIMER_MAX_CTR)
#define WHOLE_MILLISECONDS_IN_TIMER_TICK (CYCLES_IN_TIMER_TICK / CLOCK_CYCLES_IN_MS)
#define CYCLES_EXCESS (CYCLES_IN_TIMER_TICK % CLOCK_CYCLES_IN_MS)

#if CYCLES_EXCESS > 0
    volatile uint16_t _remainder_cycles = 0L;
    #define CLOCK_CYCLES_IN_HALF_MS (CLOCK_CYCLES_IN_MS / 2) 
    #define CLOCK_CYCLES_IN_HALF_MS_PLUS_ONE_TICK (CLOCK_CYCLES_IN_HALF_MS + CYCLES_EXCESS)
#endif
volatile uint32_t _milliseconds = 0L;

void configureTimer()
{
    // configuring timer 0 to use waveform generation mode
    // 010 (CTC, count to OCRA and reset)
    TCCR0A |= (1<< WGM01);

    // timer interrupt every 
    // TIMER_MAX_CTR ticks.
    OCR0A = TIMER_MAX_CTR;

    #if TIMER_DIVIDER == 1024
        TCCR0B |= ((1<<CS00) | (1<<CS02));
    #elif TIMER_DIVIDER == 256
        TCCR0B |= (1<<CS02);
    #elif TIMER_DIVIDER == 64
        TCCR0B |= ((1<<CS00) | (1<<CS01));
    #elif TIMER_DIVIDER == 8
        TCCR0B |= (1<<CS01);
    #elif TIMER_DIVIDER == 1
        TCCR0B |= (1<<CS00);
    #else
        #error Unsupported TIMER_DIVIDER value in time module
    #endif

    // Enable timer0 OCR0A compare match interrupt
    TIMSK |= (1 << OCIE0A);
}

ISR(TIMER0_COMPA_vect) {        
    #if CYCLES_EXCESS == 0
    _milliseconds += WHOLE_MILLISECONDS_IN_TIMER_TICK;
    #else
    // Creating local copy of volatile
    // _remainder_cycles so that the compiler
    // can store it in registers (each access to a volatile 
    // variable has to use memory directly.
    // This also reduces code size.            
    uint16_t remainder_cycles = _remainder_cycles;
    remainder_cycles += CYCLES_EXCESS;
    _remainder_cycles = remainder_cycles;
    if(remainder_cycles >= CLOCK_CYCLES_IN_MS)
    {
        remainder_cycles -= CLOCK_CYCLES_IN_MS;
    }
    else if (remainder_cycles>=CLOCK_CYCLES_IN_HALF_MS &&
             remainder_cycles<CLOCK_CYCLES_IN_HALF_MS_PLUS_ONE_TICK)
    {
        _milliseconds += (WHOLE_MILLISECONDS_IN_TIMER_TICK + 1);
        return;
    }
    _milliseconds += WHOLE_MILLISECONDS_IN_TIMER_TICK;
    #endif    
}

uint32_t millis()
{
    cli();
    uint32_t buf = _milliseconds;
    sei();
    return buf;
}

void getMillis(uint32_t *buf){
    cli();
    *buf = _milliseconds;
    sei();
}
