# Debugging attempts log
[[_TOC_]]

# Proper mouse plugging log

Logitech `M-U0007` plugged into the same port

dmesg:
```log
[Jan 7 20:00] usb 1-10: new low-speed USB device number 9 using xhci_hcd
[  +0.324231] usb 1-10: New USB device found, idVendor=046d, idProduct=c069, bcdDevice=56.01
[  +0.000007] usb 1-10: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[  +0.000003] usb 1-10: Product: USB Laser Mouse
[  +0.000002] usb 1-10: Manufacturer: Logitech
[  +0.023036] input: Logitech USB Laser Mouse as /devices/pci0000:00/0000:00:01.3/0000:01:00.0/usb1/1-10/1-10:1.0/0003:046D:C069.0005/input/input16
[  +0.000162] hid-generic 0003:046D:C069.0005: input,hidraw3: USB HID v1.10 Mouse [Logitech USB Laser Mouse] on usb-0000:01:00.0-10/input0
```

## Debugging attempt 1 (2022-01-07)

### Code
Commit hash `2c0fe21dbd7dfc51a4fc92697332e6e0d9e1c835`

Added `StatusLEDOn()` call into `osccal.c/calibrateOscillator()`

### Results
dmesg:
```log
[Jan 7 20:02] usb 1-10: new low-speed USB device number 10 using xhci_hcd
[  +0.224014] usb 1-10: device descriptor read/64, error -71
[  +0.304082] usb 1-10: device descriptor read/64, error -71
[  +0.236271] usb 1-10: new low-speed USB device number 11 using xhci_hcd
[  +0.223672] usb 1-10: device descriptor read/64, error -71

```
Initially status the status LED was off.
Just about when second `new low-speed USB device` message appeared,
the LED blinked and went off permanently.

### Conclusion

1. PC has detected low speed the device means D- pullup resistor works Ok.
1. Status LED went on means `calibrateOscillator()` was called =>
    `USB_RESET_HOOK` was called => `usbPoll()` was called from main()
1. Status LED probably went off because of watchdog dimer reset. 
   If this is so, then either MCU hanged somewhere in main loop
   or oscillator calibration took longer than 1s. Should try the
   same program with watchdog timer disabled.

## Debugging attempt 2 (2022-01-07)  

### Code

Commit hash `5406ba3e86f024bfcabec6ac69b80068e8729502`

Same as attempt 1 but with watchdog disabled.

### Results

dmesg (two separate device insertions):
```log
[Jan 7 20:59] usb 1-10: new low-speed USB device number 12 using xhci_hcd
[  +0.224048] usb 1-10: device descriptor read/64, error -71
[  +0.303853] usb 1-10: device descriptor read/64, error -71
[  +0.236318] usb 1-10: new low-speed USB device number 13 using xhci_hcd
[  +0.224090] usb 1-10: device descriptor read/64, error -71
[  +0.303645] usb 1-10: device descriptor read/64, error -71
[  +0.107923] usb usb1-port10: attempt power cycle
[  +0.415989] usb 1-10: new low-speed USB device number 14 using xhci_hcd
[  +0.027466] usb 1-10: Device not responding to setup address.
[  +0.236245] usb 1-10: Device not responding to setup address.
[  +0.208427] usb 1-10: device not accepting address 14, error -71
[  +0.128011] usb 1-10: new low-speed USB device number 15 using xhci_hcd
[  +0.028386] usb 1-10: Device not responding to setup address.
[  +0.235176] usb 1-10: Device not responding to setup address.
[  +0.208448] usb 1-10: device not accepting address 15, error -71
[  +0.000056] usb usb1-port10: unable to enumerate USB device
```

```log
[Jan 7 21:04] usb 1-10: new low-speed USB device number 16 using xhci_hcd
[  +0.224185] usb 1-10: device descriptor read/64, error -71
[  +0.303822] usb 1-10: device descriptor read/64, error -71
[  +0.236163] usb 1-10: new low-speed USB device number 17 using xhci_hcd
[  +0.223845] usb 1-10: device descriptor read/64, error -71
[  +0.304007] usb 1-10: device descriptor read/64, error -71
[  +0.107979] usb usb1-port10: attempt power cycle
[  +0.411977] usb 1-10: new low-speed USB device number 18 using xhci_hcd
[  +0.027997] usb 1-10: Device not responding to setup address.
[  +0.236149] usb 1-10: Device not responding to setup address.
[  +0.207857] usb 1-10: device not accepting address 18, error -71
[  +0.128000] usb 1-10: new low-speed USB device number 19 using xhci_hcd
[  +0.027378] usb 1-10: Device not responding to setup address.
[  +0.236009] usb 1-10: Device not responding to setup address.
[  +0.208600] usb 1-10: device not accepting address 19, error -71
[  +0.000040] usb usb1-port10: unable to enumerate USB device
```

Status LED went on and stayed on.

### Additional info

Someone had similar problems with Digispark and micronucleus
- https://github.com/wenerme/wener/blob/8d3a388647a675d59f4e8299bec5f14dbff97bc2/notes/hardware/microcontroller/avr/digispark/README.md
- https://github.com/micronucleus/micronucleus/issues/255


Linux error codes and USB-related code
- https://kernel.googlesource.com/pub/scm/linux/kernel/git/nico/archive/+/v0.97/include/linux/errno.h
  ```c
  #define	EPROTO		71	/* Protocol error */
  ```
- https://github.com/torvalds/linux/blob/1bff7d7e8c487b9b0ceab70b43b781f1d45f55eb/drivers/usb/core/hub.c#L4842
  ```c
    /* Retry on all errors; some devices are flakey.
    * 255 is for WUSB devices, we actually need to use
    * 512 (WUSB1.0[4.8.1]).
    */
    for (operations = 0; operations < GET_MAXPACKET0_TRIES;
            ++operations) {
        buf->bMaxPacketSize0 = 0;
        r = usb_control_msg(udev, usb_rcvaddr0pipe(),
            USB_REQ_GET_DESCRIPTOR, USB_DIR_IN,
            USB_DT_DEVICE << 8, 0,
            buf, GET_DESCRIPTOR_BUFSIZE,
            initial_descriptor_timeout);
        switch (buf->bMaxPacketSize0) {
        case 8: case 16: case 32: case 64: case 255:
            if (buf->bDescriptorType ==
                    USB_DT_DEVICE) {
                r = 0;
                break;
            }
            fallthrough;
        default:
            if (r == 0)
                r = -EPROTO;
            break;
        }
        /*
        * Some devices time out if they are powered on
        * when already connected. They need a second
        * reset. But only on the first attempt,
        * lest we get into a time out/reset loop
        */
        if (r == 0 || (r == -ETIMEDOUT &&
                retries == 0 &&
                udev->speed > USB_SPEED_FULL))
            break;
    }
  ```


### Conclusion

1. Status LED turning off during attempt 1 indeed was 
   caused by watchdog reset and not by MCU hanging.
1. Need to reposition `StatusLEDOn()` to
   1. make sure that the device can detect any signals
      from PC at all
   1. make sure that oscillator calibration succeeds
   1. get an idea of oscillator frequency
      (for example measure main loop frequency)      
   1. detect which other part of USB implementation
      might be failing


## Debugging attempt 3 (2022-01-08)      

### Code

Commit hash `9ccb15a1bf181600f390c9d948a03a41d50d1937`

Same as attempt 2 but
- `StatusLEDOn()` call moved to end of `calibrateOscillator()`
- HID report descriptor length discrepancy fixed. `USB_CFG_HID_REPORT_DESCRIPTOR_LENGTH` macro in `usbconfig.h` did not match actual usbHidReportDescriptor array length in `main.c`


### Results

dmesg:
```log
[Jan 8 13:24] usb 1-10: new low-speed USB device number 3 using xhci_hcd
[  +0.224036] usb 1-10: device descriptor read/64, error -71
[  +0.304011] usb 1-10: device descriptor read/64, error -71
[  +0.235949] usb 1-10: new low-speed USB device number 4 using xhci_hcd
[  +0.224032] usb 1-10: device descriptor read/64, error -71
[  +0.244021] usb 1-10: device descriptor read/64, error -71
[  +0.108000] usb usb1-port10: attempt power cycle
[  +0.412340] usb 1-10: new low-speed USB device number 5 using xhci_hcd
[  +0.028019] usb 1-10: Device not responding to setup address.
[  +0.235142] usb 1-10: Device not responding to setup address.
[  +0.208612] usb 1-10: device not accepting address 5, error -71
[  +0.128305] usb 1-10: new low-speed USB device number 6 using xhci_hcd
[  +0.028020] usb 1-10: Device not responding to setup address.
[  +0.235314] usb 1-10: Device not responding to setup address.
[  +0.208212] usb 1-10: device not accepting address 6, error -71
[  +0.000057] usb usb1-port10: unable to enumerate USB device
```

Status led remained off

### Conclusion

1. Oscillator calibration did not finish
   - either the device failed to detect SOF signal 
     repeating every millisecond after reset
   - or the device hanged
   - or the hub does not behave as V-USB oscillator calibration
     code expects it to
2. Need to determine if any SOF signals are detected at all

## Debugging attempt 4 (2022-01-08)      

### Code

Commit hash `2379d6526ff7e9e82f0563cf794b8daa25e7b85c`

Same as attempt 3 but `StatusLEDOn()` into binary search loop
inside `calibrateOscillator()` (right after `usbMeasureFrameLength()` call).

### Results

dmesg is the same as before.
```log
[Jan 8 13:45] usb 1-10: new low-speed USB device number 12 using xhci_hcd
[  +0.224191] usb 1-10: device descriptor read/64, error -71
[  +0.303840] usb 1-10: device descriptor read/64, error -71
[  +0.235989] usb 1-10: new low-speed USB device number 13 using xhci_hcd
[  +0.224203] usb 1-10: device descriptor read/64, error -71
[  +0.303845] usb 1-10: device descriptor read/64, error -71
[  +0.107990] usb usb1-port10: attempt power cycle
[  +0.411954] usb 1-10: new low-speed USB device number 14 using xhci_hcd
[  +0.027386] usb 1-10: Device not responding to setup address.
[  +0.232003] usb 1-10: Device not responding to setup address.
[  +0.208778] usb 1-10: device not accepting address 14, error -71
[  +0.127855] usb 1-10: new low-speed USB device number 15 using xhci_hcd
[  +0.027663] usb 1-10: Device not responding to setup address.
[  +0.235716] usb 1-10: Device not responding to setup address.
[  +0.208633] usb 1-10: device not accepting address 15, error -71
[  +0.000058] usb usb1-port10: unable to enumerate USB device
```

Status LED went on and stayed on

## Conclusion

1. At least first call to `usbMeasureFrameLength()` inside 
   `calibrateOscillator()` did not hang. But this does not
   mean it was able to detect any SOF signals. It may have
   returned due to timeout.

## Debugging attempt 5 (2022-01-08)

### Code

Commit hash `f9fe20a782d822b40d620101109fa466dd8962c4`

Same as attempt 4 but status LED turns on
if `usbMeasureFrameLength()` is greater than
predefined constant. 
By systematically trying different values 
of the constant last measured `usbMeasureFrameLength()`
after binary search in `calibrateOscillator()`
can be determined.

Commit hash `9b1b0e51fb84073b2b73fad4487d0583c95a7074`

similar to above but LED is turned on if 
the last `usbMeasureFrameLength()` value is less (!) 
than the first.

### Results

Trying to determine frame length

| x (in units of 7 CPU cycles) | usbMeasureFrameLength() >= x |
| ---------------------------- | ---------------------------- |
| 2356(*)                      | no                           |
| 1000                         | no                           |
| 500                          | no                           |
| 100                          | no                           |
| 10                           | yes                          |
| 50                           | yes                          |
| 75                           | yes                          |

(*) 2356 is the target value of frame length
    for `F_CPU` of `16.5 MHz`

### Conclusion

- `usbMeasureFrameLength()` returned non-zero value
  => some SOF packets were detected. 
- with `OSCCAL = 255` (max frequency)
  frame interval that is supposedly 1 ms
  is measured by `usbMeasureFrameLength()` as 
  `(76..99)*7 = 532..693` CPU cycles.
  Meaning real `F_CPU` is `0.53..0.69 MHz`.
  This is obviously wrong. `F_CPU` should 
  be at least `1 MHz`
- measured frame length decreases (!) with increasing `OSCCAL`.
  Don't know how this can be explained. Something
  in device operation radically differs from my expectations. 

## Debugging attempt 6 (2022-01-08)

### Code

Commit hash `3adcc067eaa3aa1c91afa8a5796c4f366bcfd203`

Similar to `9b1b0e51fb84073b2b73fad4487d0583c95a7074` in 
debugging attempt 5 but status LED turns on if
frame length increases with increasing `OSCCAL`.

Most importantly `CKDIV8` fuse is set to unprogrammed
(previously it was programmed which is the default).
Now the device should run at `8 MHz` instead of `1 MHz`
by default. 

### Results

dmesg output has not changed. Device still not recognized.

Status LED turns on and stays on.

### Conclusion

- Previous troubles with RC oscillator calibration were 
  at least in part caused by wrong `CKDIV8` fuse setting.
- Now USB frame length rises with increasing `OSCCAL` as
  expected.
- Device still not recognized. Need to determine 
  measured frame length as in debugging attempt 5
  to draw further conclusions

## Debugging attempt 7 (2022-01-08)

### Code

Commit hash `d1e018992e5a2970e760c3b1f5bebc2d3cefd1ce`

Same as `f9fe20a782d822b40d620101109fa466dd8962c4` 
in debugging attempt 5 but with correct 
`CKDIV8` fuse setting

Commit `d98660c707362495912432c9fb2edeb853a5cb49`
Outputs `OSCCAL` and measured frame length 
with status LED "optical telegraph" decoded 
from video taken on smartphone with scripts
that can be found [here](https://gitlab.com/self-moving-mouse/self-moving-mouse.tools/-/tree/master/led-video-to-binary)

### Results

| x (in units of 7 CPU cycles) | usbMeasureFrameLength() > x  |
| ---------------------------- | ---------------------------- |
| 2356(*)                      | no                           |
| 1000                         | yes                          |
| 2000                         | yes                          |
| 2200                         | no                           |
| 2100                         | no                           |

(*) 2356 is the target value of frame length
    for `F_CPU` of `16.5 MHz`

LED "optical telegraph" results

![](debug7-led-signal.png)

`OSCCAL = 11111111b = 255` 

`usbMeasureFrameLength() = 0000011111101010b = 2026`

### Conclusions

- Assuming real frame length is `1 ms`
  real `F_CPU` is `2026*7/0.001 = 14.182 MHz`.  
- I should have set `CKSEL` to 
  "High Frequency PLL Clock" (`0001`, see
  section 6.2 of ATTiny datasheet).
  This perfectly explains why `OSCCAL = 255`
  resulted in `F_CPU` of only about `14 MHz` (
  see figure 22-42 in ATTiny datasheet).

  https://codeandlife.com/2012/02/22/v-usb-with-attiny45-attiny85-without-a-crystal/

## Debugging attempt 8 (2022-01-09)

### Code

Commit hash `3900e9ef44e011e8362a0a9da407b162a4246fed`

Same as `9ccb15a1bf181600f390c9d948a03a41d50d1937` 
in debugging attempt 3 but with `CKSEL` fuse fixed.

### Results

dmesg:

```log
[Jan 9 19:13] usb 1-10: new low-speed USB device number 8 using xhci_hcd
[  +0.333100] usb 1-10: New USB device found, idVendor=16c0, idProduct=27da, bcdDevice= 1.02
[  +0.000008] usb 1-10: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[  +0.000003] usb 1-10: Product: SeMo
[  +0.000002] usb 1-10: Manufacturer: example.com
[  +0.000002] usb 1-10: SerialNumber: example.com:Mouse
[  +0.021947] input: example.com SeMo as /devices/pci0000:00/0000:00:01.3/0000:01:00.0/usb1/1-10/1-10:1.0/0003:16C0:27DA.0004/input/input15
[  +0.000145] hid-generic 0003:16C0:27DA.0004: input,hidraw3: USB HID v1.01 Mouse [example.com SeMo] on usb-0000:01:00.0-10/input0
[  +9.200651] usb 1-10: USB disconnect, device number 8
```

Calibration was successful.
Mouse was recognized and moved the
cursor in circles as expected.

### Conclusion

- I should have payed more attention to ATTiny datasheet
  and V-USB example projects. Everything was there.
- Finally I can return to translating self-moving mouse from
  "arduinese" into "raw" C



  
