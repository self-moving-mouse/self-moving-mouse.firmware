#ifndef __statusled_h_guard__
#define __statusled_h_guard__

#define STATUS_LED_PIN                (1 << PB0)
#define STATUS_LED_DATA_REGISTER      PORTB
#define STATUS_LED_DIRECTION_REGISTER DDRB

#define ConfigureStatusLED() STATUS_LED_DIRECTION_REGISTER |= STATUS_LED_PIN;
#define StatusLEDOn()  STATUS_LED_DATA_REGISTER |= (STATUS_LED_PIN)
#define StatusLEDOff() STATUS_LED_DATA_REGISTER &= (~(STATUS_LED_PIN))
#define StatusLEDToggle() STATUS_LED_DATA_REGISTER ^= (STATUS_LED_PIN)


#endif /* __statusled_h_guard__ */

