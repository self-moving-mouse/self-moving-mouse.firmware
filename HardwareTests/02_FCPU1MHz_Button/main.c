#include <avr/io.h>
#include <util/delay.h>

#define STATUS_LED_PIN                (1 << PB0)
#define STATUS_LED_DATA_REGISTER      PORTB
#define STATUS_LED_DIRECTION_REGISTER DDRB

#define StatusLEDOn()  STATUS_LED_DATA_REGISTER |= (STATUS_LED_PIN)
#define StatusLEDOff() STATUS_LED_DATA_REGISTER &= (~(STATUS_LED_PIN))
#define StatusLEDToggle() STATUS_LED_DATA_REGISTER ^= (STATUS_LED_PIN)

#define BUTTON_PIN                (1 << PB1)
#define BUTTON_DATA_REGISTER      PORTB
#define BUTTON_INPUT_REGISTER     PINB
#define BUTTON_DIRECTION_REGISTER DDRB

#define ButtonIsUp() (BUTTON_INPUT_REGISTER & BUTTON_PIN)


inline void configureIOPins()
{
    // configuring status led pin for output
    STATUS_LED_DIRECTION_REGISTER |= STATUS_LED_PIN;

    // configuring button pin for input
    BUTTON_DIRECTION_REGISTER &= ~BUTTON_PIN;
    // disabling button pin built-in pullup
    BUTTON_DATA_REGISTER &= ~BUTTON_PIN;
}

int __attribute__((noreturn)) main(void)
{
    configureIOPins();

    while(1){        
        if(ButtonIsUp()){
            StatusLEDOff();
        } else {
            StatusLEDOn();
        }
    }
}
