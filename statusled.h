#ifndef __statusled_h_guard__
#define __statusled_h_guard__

#include <avr/io.h>

#define STATUS_LED_PIN                (1 << PB0)
#define STATUS_LED_DATA_REGISTER      PORTB
#define STATUS_LED_DIRECTION_REGISTER DDRB

#define configureStatusLED() STATUS_LED_DIRECTION_REGISTER |= STATUS_LED_PIN

#define statusLEDOn()  STATUS_LED_DATA_REGISTER |= (STATUS_LED_PIN)
#define statusLEDOff() STATUS_LED_DATA_REGISTER &= (~(STATUS_LED_PIN))
#define statusLEDToggle() STATUS_LED_DATA_REGISTER ^= (STATUS_LED_PIN)

#define getStatusLEDState() (STATUS_LED_DATA_REGISTER & (STATUS_LED_PIN))

#endif /* __statusled_h_guard__ */