#ifndef DEBOUNCING_HEADER_GUARD
#define DEBOUNCING_HEADER_GUARD

#include <stdint.h>

// Debouncing strategy:
// 1. PinState->StateCurrent is updated on pin change
//    interrupt with UpdatePinState(..) function.
//    After each change PinState->lastChangeTime
//    is set to current time.
//    UpdatePinState(..) can also be called form
//    main loop if interrupt is not an option for some
//    reason
// 2. State change is analyzed and acted upon in
//    main loop. After state change has been processed
//    PinState->StateOld should be set to
//    PinState->StateCurrent (see MarkStateAsProcessed(..))

// Assumes the following pushbutton connection scheme
//        ^ VCC
//        |
//       | |
//       |R|
//       | |    SW
//        |    __|__
//  pin --*-----   ------ GND
//
// Short button press is registered when pin had been
// LOW for less than longPressDuration, then became
// HIGH and stayed that way for more than
// debouncingThreshold
//
// Long button press is registered when pin had been
// HIGH then became LOW and stayed that way for longer
// than longPressDuration

#define NO_UNPROCESSED_EVENTS      0
#define SHORT_BUTTON_PRESS         1
#define LONG_BUTTON_PRESS          2

typedef struct PinState {
    volatile uint8_t StateOld;
    volatile uint8_t StateCurrent;
    volatile uint32_t lastChangeTime;
    uint8_t longPressStatus;
} PinState;

void UpdatePinState(
  PinState* pinState,
  uint8_t newValue,
  uint32_t currentTime);

int8_t GetUnprocessedEvent(
  PinState* pinState,
  uint32_t debouncingThreshold,
  uint32_t longPressDuration,
  uint32_t currentTime);

void MarkStateAsProcessed(
  PinState* pinState);
#endif
