#ifndef HEADER_GUARD_MICRO_RNG
#define HEADER_GUARD_MICRO_RNG

#include <stdint.h>

// Random number generator was adapted from
// https://gist.github.com/endolith/2568571
// by endolith (https://gist.github.com/endolith)
//
// The gist of the algorithm is using
// frequency and phase mismatch between two independent
// clocks: main clock of the controller and watchdog
// timer to gather randomness. Circular shifting and
// xor-ing distributes randomness between all bits of the
// result.

typedef struct RNGState {
    volatile uint8_t sample;
    volatile uint8_t sample_waiting;
    uint8_t current_bit;
    uint8_t random_number;
} RNGState;

#ifndef TEST
void configureRNG();
#endif

// this procedure should be called from watchdog 
// interrupt handler. Current value of one of 
// MCU's hardware timers should be passed as sample 
// argument value
void RNGOnWatchdog(
    RNGState* rngState,
    uint8_t sample);

// This method should be called preferably at
// non-deterministic intervals to accumulate randomness.
// Each call adds 1 bit of randomness if random sample 
// collected by RNGOnWatchdog() is ready until
// all 8 bits are ready.
// Returns true when next random number is ready.
uint8_t updateRandomSample(RNGState* rngState);

// returns random number and resumes randomness 
// accumulation process for the next one
uint8_t useRandomNumber(RNGState* rngState);

#endif // HEADER_GUARD_MICRO_RNG
