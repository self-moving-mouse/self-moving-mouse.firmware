FROM gitpod/workspace-full

RUN sudo apt-get update  && sudo apt-get install -y build-essential gdb gcc-avr avr-libc avrdude python3-pyparsing python3-junit.xml gcovr && sudo rm -rf /var/lib/apt/lists/*
