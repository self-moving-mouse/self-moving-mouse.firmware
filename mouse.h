#ifndef MOUSE_HEADER_GUARD
#define MOUSE_HEADER_GUARD

#include <stdint.h>

// Schedules cursor
// movement sending to host.
// If previously scheduled
// movement was not sent it
// will be lost.
void setDxDy(
    int8_t dx, 
    int8_t dy);

// If previously scheduled
// movement has been already
// sent or no movement has been
// scheduled for sending yet, 
// schedules new movement for sending
// to host and returns 1
//
// If previously scheduled 
// movement has not been sent to host
// returns 0
#ifdef ENABLE_trySetDxDy
uint8_t trySetDxDy(
    int8_t dx, 
    int8_t dy);
#endif    

#endif