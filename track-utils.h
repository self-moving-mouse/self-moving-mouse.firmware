#ifndef HEADER_GUARD_TRACK_UTILS
#define HEADER_GUARD_TRACK_UTILS

// This module assumes that DDPCM_DELTA is 
// a difference table for DDPCM comression 
// in progmem

typedef struct TrackPlayerState {
    PGM_VOID_P track;
    uint8_t trackSize;
    uint8_t currentStep;
    int8_t currentDX;
    int8_t currentDY;
} TrackPlayerState;

// Sets new track to play
void setTrack(
    TrackPlayerState* state, 
    PGM_VOID_P track, 
    uint8_t trackLength);

// Plays next track step (if any).
// Returns false if end of track is reached
uint8_t playNextStep(
    TrackPlayerState* state);

#endif
